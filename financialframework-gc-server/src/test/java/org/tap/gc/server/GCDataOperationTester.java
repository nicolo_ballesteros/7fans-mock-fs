package org.tap.gc.server;

import java.beans.PropertyVetoException;
import java.io.File;
import java.util.UUID;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tap.gc.OpCodeConstants;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.RecallTxnNo;
import org.tap.gc.beans.Transaction;
import org.tap.gc.server.tasks.GCDataOperation;
import org.tap.server.ServerException;
import org.tap.server.datasource.DataSourceServer;
import org.tap.server.datasource.c3p0impl.C3P0DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class GCDataOperationTester {
	private DataSourceServer server;
	
	public GCDataOperationTester() throws PropertyVetoException{
		org.apache.log4j.BasicConfigurator.configure();
		server = createDataSourceServer(); 
	}
	
	@Before
	public void setUp() throws ServerException{
		server.startup();
	}
	
	@After
	public void tearDown() throws ServerException{
		server.shutdown();
	}
	
	@Test
	public void testSave() {
		GCDataOperation operation = new GCDataOperation();
		Interface data = new Interface();
		data.setOpCode(OpCodeConstants.RedeemCouponCode);
		Transaction txn = new Transaction();
		txn.setAmount(Long.parseLong("10000"));
		txn.setApprovalCode("000001");
		txn.setBalance(Long.parseLong("10000"));
		txn.setBusDate("2012-02-10");
		txn.setCashierId("00001");
		txn.setCouponCount(1);
		txn.setCouponUID("99990000001111");
		txn.setExpiryDate("2014-09-08");
		txn.setServerId("01");
		txn.setSN("0102032012071000010001");
		txn.setTxnNo("010203201207100001");
		txn.setStoreId("02");
		txn.setTxnDate("2012-7-10 12:03:38");
		txn.setTxnType("RedeemCoupon");
		txn.setUserId("002");
		data.setTransaction(txn);
		
		operation.operate(data);
	}
	
	@Test
	public void testSave2() throws JAXBException{
		GCDataOperation operation = new GCDataOperation();
		Interface inter = (Interface)XmlHelper.unmarshaller(new File("src/test/resources/offline_sellcoupon.xml"));
		operation.operate(inter);
		
	}
	
	@Test
	public void testConfirm(){
		GCDataOperation operation = new GCDataOperation();
		Interface data = new Interface();
		data.setOpCode(OpCodeConstants.ConfirmCode);
		Transaction txn = new Transaction();
		txn.setTxnNo("010203201207100001");
		data.setTransaction(txn);
		
		operation.operate(data);
	}
	
	@Test
	public void testRecall(){
		GCDataOperation operation = new GCDataOperation();
		Interface data = new Interface();
		data.setOpCode(OpCodeConstants.SellCouponCode);
		Transaction txn = new Transaction();
		final String txnNo = UUID.randomUUID().toString();
		txn.setAmount(Long.parseLong("10000"));
		txn.setApprovalCode("000001");
		txn.setBalance(Long.parseLong("10000"));
		txn.setBusDate("2012-02-10");
		txn.setCashierId("00001");
		txn.setCouponCount(1);
		txn.setCouponUID("99990000001111");
		txn.setExpiryDate("2014-09-08");
		txn.setServerId("01");
		txn.setSN(txnNo + "001");
		txn.setTxnNo(txnNo);
		txn.setStoreId("02");
		txn.setTxnDate("2012-7-10 12:03:38");
		txn.setTxnType("SellCoupon");
		txn.setUserId("002");
		data.setTransaction(txn);
		
		operation.operate(data);
		
		System.out.println("TxnNo:" + txnNo);
		
		final String newTxnNo = UUID.randomUUID().toString();
		data = new Interface();
		data.setOpCode(OpCodeConstants.RecallTxnCode);
		RecallTxnNo recall = new RecallTxnNo();
		recall.setOldTxnNo(txnNo);
		recall.setNewTxnNo(newTxnNo);
		data.setRecallTxnNo(recall);
		operation.operate(data);
		
		System.out.println("OldTxnNo:" + txnNo);
		System.out.println("NewTxnNo:" + newTxnNo);
	}
	
	public DataSourceServer createDataSourceServer() throws PropertyVetoException{
		return new DataSourceServer(createDataSource());
	}

	public C3P0DataSource createDataSource() throws PropertyVetoException{
		ComboPooledDataSource pool = new ComboPooledDataSource();
		pool.setDriverClass("com.microsoft.jdbc.sqlserver.SQLServerDriver");
		pool.setJdbcUrl("jdbc:microsoft:sqlserver://tapsh-sqlserver\\sqlexpress:1433;DatabaseName=EFTPaymentData");
		pool.setUser("sa");
		pool.setPassword("1234qweR");
		pool.setInitialPoolSize(1);
		pool.setMinPoolSize(2);
		pool.setMaxPoolSize(10);
		pool.setMaxIdleTime(60);
		pool.setAcquireIncrement(5);
		pool.setIdleConnectionTestPeriod(60);
		C3P0DataSource ds = new C3P0DataSource(pool);
		return ds;
	}
}
