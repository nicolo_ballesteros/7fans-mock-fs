package org.tap.gc.server;

import java.io.StringReader;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.tap.communication.IPacket;
import org.tap.communication.Receipt;
import org.tap.gc.ErrorCode;
import org.tap.gc.OpCodeConstants;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.HQLoyaltyInfo;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Page;
import org.tap.gc.security.ISecurity;
import org.tap.gc.security.support.NonSecurity;

import com.tap.operation.IOperation;
import com.tap.operation.OperationBase;
import com.tap.peripheralsframework.IHQLoyaltyAction;
import com.tap.peripheralsframework.support.hqloyalty.HQLoyaltyException;
import com.tap.peripheralsframework.support.hqloyalty.HQLoyaltyStatus;
import com.tap.peripheralsframework.support.hqloyalty.beans.accountsactivity.Account;
import com.tap.peripheralsframework.support.hqloyalty.beans.accountsactivity.Accounts;
import com.tap.peripheralsframework.support.hqloyalty.beans.demographic.Card;
import com.tap.peripheralsframework.support.hqloyalty.beans.demographic.HouseHold;
import com.tap.peripheralsframework.support.hqloyalty.beans.demographic.Member;

public class HQLoyaltyOperation extends OperationBase {

	private static Logger logger = Logger.getLogger(GCOperation.class);
	private BufferData bf;
	private IHQLoyaltyAction hqAction;
	private String encoding = "UTF-8";
	private ISecurity security;

	private String hqUsername;
	private String hqPassword;
	private int accountId = 0;

	private final String SERVICE_MESSAGE = "ServiceMessage";
	private final String ERROR_CODE = "ErrorCode";
	private final String RESPONSE = "Response";
	private final int FIRST_ACCOUNT = 0;
	private final int MIN_ACCOUNT_ID = -1;
	private final int MAX_ACCOUNT_ID = -2;

	public HQLoyaltyOperation(IHQLoyaltyAction hqLoyaltyAction) {
		this.hqAction = hqLoyaltyAction;
		bf = new BufferData();
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getHqUsername() {
		return hqUsername;
	}

	public void setHqUsername(String hqUsername) {
		this.hqUsername = hqUsername;
	}

	public String getHqPassword() {
		return hqPassword;
	}

	public void setHqPassword(String hqPassword) {
		this.hqPassword = hqPassword;
	}

	public void setSecurity(ISecurity security) {
		this.security = security;
	}

	public ISecurity getSecurity() {
		if (null == security)
			security = new NonSecurity();
		return security;
	}

	@Override
	public Object operate(Object info) {
		if (!(info instanceof IPacket))
			throw new IllegalArgumentException("info is not an instance of IPacket.");
		IPacket packet = (IPacket) info;
		Interface request = null;
		Interface response;
		String requestXml;
		try {
			requestXml = getSecurity().decode((String) packet.getReceived(), encoding);
			logger.debug("FF request data: " + requestXml);
			System.out.println("request :" + requestXml);
			request = (Interface) XmlHelper.unmarshaller(requestXml);
			logger.debug("Operating OpCode: " + request.getOpCode());

			fillStoreCode(request);
			fillBrandCode(request);
			fillPage(request);

			response = processHQAction(request);

			String res = XmlHelper.marshaller(response);
			Receipt receipt = new Receipt(packet, getSecurity().encode(res, encoding));
			receipt.setBusiness("GC");
			receipt.setAction(response.getOpCodeDescription());

			return receipt;
		} catch (JAXBException e) {
			e.printStackTrace();
			logger.error("Operated OpCode: " + request.getOpCode() + " failure, exception: " + e.getMessage());
			logger.error(e.getMessage(), e);
			return this.handleConfirmException(packet, request, e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Operated OpCode: " + request.getOpCode() + " failure, exception: " + e.getMessage());
			logger.error(e.getMessage(), e);
			return this.handleConfirmException(packet, request, e);
		}

	}

	private Interface processHQAction(Interface request) {

		String sessionKey = null;
		String cardNumber = request.getHQLoyaltyInfo().getCardNumber();
		int cardStatus = 0;
		String expiryDate = "";
		String firstName = "";
		String lastName = "";
		float pointBalance = 0;
		String lastPurchase = "";

		Interface responseInterface = new Interface();
		responseInterface.setOpCode(request.getOpCode());
		responseInterface.setResponseCode("0");
		responseInterface.setBusinessType(request.getBusinessType());
		HQLoyaltyInfo info = new HQLoyaltyInfo();
		responseInterface.setHQLoyaltyInfo(info);

		try {
			String response, errorCode;
			JSONObject loginObj, cardValidateObj, demographicObj, accountActivityObj;
			// JAXBContext jc = JAXBContext.newInstance(ObjectFactory.class);
			// JAXBContext jcCardValidate = JAXBContext.newInstance(com.tap.peripheralsframework.support.hqloyalty.beans.cardvalidate.ObjectFactory.class);
			JAXBContext jcDemographic = JAXBContext.newInstance(com.tap.peripheralsframework.support.hqloyalty.beans.demographic.ObjectFactory.class);
			JAXBContext jcAccountsActivity = JAXBContext.newInstance(com.tap.peripheralsframework.support.hqloyalty.beans.accountsactivity.ObjectFactory.class);
			String xmlStr;

			// call userLogin() -> get sessionKey
			logger.debug("exec login");
			response = hqAction.userLogin(hqUsername, hqPassword);
			loginObj = new JSONObject(response);
			errorCode = loginObj.getString(ERROR_CODE);
			logger.debug("login response: " + loginObj.toString());

			if (!errorCode.equals(HQLoyaltyStatus.SUCCESS) || loginObj == null || loginObj.equals("")) {
				// userLogin() not success
				responseInterface.setResponseCode(ErrorCode.CODE_REMASERVICEERROR);

			} else {
				// userLogin() success -> get sessionKey
				// call cardValidate()
				sessionKey = loginObj.getString(RESPONSE);

				logger.debug("exec cardValidate");
				response = hqAction.cardValidate(sessionKey, cardNumber);
				cardValidateObj = new JSONObject(response);
				errorCode = cardValidateObj.getString(ERROR_CODE);
				logger.debug("cardValidate response: " + response.toString());

				if (!errorCode.equals(HQLoyaltyStatus.SUCCESS) || cardValidateObj == null || cardValidateObj.toString().equals("")) {
					// cardValidate() not success
					responseInterface.setResponseCode(ErrorCode.CODE_CARDVALIDATIONERROR);
					responseInterface.setErrorMessage(cardValidateObj.getString(SERVICE_MESSAGE));

				} else {
					// cardValidate() success -> get cardStatus
					// call getDemographic()
					logger.debug("exec getDemographic");
					response = hqAction.getDemographic(sessionKey, cardNumber);
					demographicObj = new JSONObject(response);
					errorCode = demographicObj.getString(ERROR_CODE);
					logger.debug("getDemographic response: " + response.toString());

					if (!errorCode.equals(HQLoyaltyStatus.SUCCESS) || demographicObj == null || demographicObj.toString().equals("")) {
						// getDemographic() not success
						responseInterface.setResponseCode(ErrorCode.CODE_GETDEMOGRAPHICERROR);
						responseInterface.setErrorMessage(demographicObj.getString(SERVICE_MESSAGE));

					} else {
						// getDemographic() success -> get expiryDate, memberName
						// call getHouseHoldAccountsActivity()
						xmlStr = demographicObj.getString(RESPONSE);
						HouseHold houseHold = (HouseHold) jcDemographic.createUnmarshaller().unmarshal(new StringReader(xmlStr));
						if (houseHold != null) {
							for (Member member : houseHold.getMembers().getMembers()) {
								for (Card card : member.getCards().getCards()) {
									if (card.getId().equals(cardNumber)) {
										cardStatus = card.getCardStatus();
										expiryDate = card.getExpirationDate().substring(0, 10);
										break;
									}
								}
								firstName = member.getFirstName() == null ? "" : member.getFirstName();
								lastName = member.getLastName() == null ? "" : member.getLastName();
							}
						}

						logger.debug("exec getHouseHoldAccountsActivity");
						response = hqAction.getHouseHoldAccountsActivity(sessionKey, cardNumber);
						accountActivityObj = new JSONObject(response);
						errorCode = accountActivityObj.getString(ERROR_CODE);
						logger.debug("getHouseHoldAccountsActivity response: " + response.toString());

						if (!errorCode.equals(HQLoyaltyStatus.SUCCESS) || accountActivityObj == null || accountActivityObj.toString().equals("")) {
							// getHouseHoldAccountsActivity() not success
							responseInterface.setResponseCode(ErrorCode.CODE_GETACCOUNTSACTIVITYERROR);
							responseInterface.setErrorMessage(accountActivityObj.getString(SERVICE_MESSAGE));

						} else {
							// getHouseHoldAccountsActivity() success -> get pointBalance, lastPurchaseDate
							xmlStr = accountActivityObj.getString(RESPONSE);
							Accounts accounts = (Accounts) jcAccountsActivity.createUnmarshaller().unmarshal(new StringReader(xmlStr));
							if (accounts != null) {
								if (accounts.getAccounts() != null) {
									int tmpId = 0;
									for (Account account : accounts.getAccounts()) {
										if (accountId == FIRST_ACCOUNT) {
											pointBalance = account.getBalance();
											lastPurchase = account.getLastUpdate().substring(0, 10);
											break;
										} else if (accountId == MIN_ACCOUNT_ID) {
											if (tmpId == 0 || account.getId() < tmpId) {
												tmpId = account.getId();
												pointBalance = account.getBalance();
												lastPurchase = account.getLastUpdate().substring(0, 10);
											}
										} else if (accountId == MAX_ACCOUNT_ID) {
											if (tmpId == 0 || account.getId() > tmpId) {
												tmpId = account.getId();
												pointBalance = account.getBalance();
												lastPurchase = account.getLastUpdate().substring(0, 10);
											}
										} else if (accountId == account.getId()) {
											pointBalance = account.getBalance();
											lastPurchase = account.getLastUpdate().substring(0, 10);
											break;
										}
									}
								}
							}
							info.setPointBalance(pointBalance);
							info.setCardNumber(cardNumber);
							info.setCardStatus(cardStatus);
							info.setExpiryDate(expiryDate);
							info.setLastPurchaseDate(lastPurchase);
							info.setFirstName(firstName);
							info.setLastName(lastName);
						}
					}
				}
			}
		} catch (HQLoyaltyException e) {
			responseInterface.setResponseCode(ErrorCode.CODE_REMASERVICEERROR);
			logger.error(e);
		} catch (JSONException e) {
			responseInterface.setResponseCode(ErrorCode.CODE_PARSEJSONERROR);
			logger.error(e);
		} catch (JAXBException e) {
			responseInterface.setResponseCode(ErrorCode.CODE_UNMARSHALLERERROR);
			logger.error(e);
		} catch (Exception e) {
			responseInterface.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
			logger.error(e);
		}

		return responseInterface;
	}

	private Receipt handleConfirmException(IPacket packet, Interface request, Exception e) {
		logger.debug("exec handleConfirmException...");
		int opcode = request.getOpCode();
		// String responseCode = request.getResponseCode();
		String busType = request.getBusinessType();
		Receipt receipt = null;

		try {
			if (opcode == OpCodeConstants.CouponInfoCode) {
				logger.error(e);

				if (!busType.equals(OpCodeConstants.BUSINESSTYPE_RRC)) {

					Interface newInterface = request;
					String errorInfo = "";
					if (e != null) {
						errorInfo = e.toString();
					}
					if (errorInfo == null) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_SYSTEMERROR);

					} else if (errorInfo.contains("javax.xml.bind.UnmarshalException")) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_UNMARSHALLERERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_UNMARSHALLERERROR);

					} else if (errorInfo.contains("javax.xml.bind.MarshalException")) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_MARSHALLERERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_MARSHALLERERROR);

					} else if (errorInfo.contains("Read timed out") || e.getMessage().contains("Executed timeout")) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_READTIMEOUTERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_READTIMEOUTERROR);

					} else if (errorInfo.contains("org.apache.axis2.AxisFault") || e.getMessage().contains("java.net.ConnectException")
							|| e.getMessage().contains("java.net.SocketException")) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);

					} else if (errorInfo.contains("java.net.NoRouteToHostException")) {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_NETWORKCABLEERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_NETWORKCABLEERROR);

					} else {
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
					}

					request.setErrorMessage("Success");
					request.setResponseCode("0");
				}
			} else {
				String errorInfo = "";
				if (e != null) {
					errorInfo = e.toString();
				}
				System.out.println("errorInfo ==" + errorInfo);
				if (errorInfo == null) {
					logger.debug("is null");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_SYSTEMERROR);

				} else if (errorInfo.contains("javax.xml.bind.UnmarshalException")) {
					logger.debug("is javax.xml.bind.UnmarshalException");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_UNMARSHALLERERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_UNMARSHALLERERROR);

				} else if (errorInfo.contains("javax.xml.bind.MarshalException")) {
					logger.debug("is javax.xml.bind.MarshalException");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_MARSHALLERERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_MARSHALLERERROR);

				} else if (errorInfo.contains("Read timed out") || e.getMessage().contains("Executed timeout")) {
					logger.debug("is Read timed out");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_READTIMEOUTERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_READTIMEOUTERROR);

				} else if (errorInfo.contains("org.apache.axis2.AxisFault") || e.getMessage().contains("java.net.ConnectException")
						|| e.getMessage().contains("java.net.SocketException")) {
					logger.debug("is org.apache.axis2.AxisFault");

					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);

				} else if (errorInfo.contains("java.net.NoRouteToHostException")) {
					logger.debug("is java.net.NoRouteToHostException");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_NETWORKCABLEERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_NETWORKCABLEERROR);

				} else {
					logger.debug("is system error");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
				}
			}

			String res = XmlHelper.marshaller(request);
			logger.debug("FS response data:" + res);
			receipt = new Receipt(packet, getSecurity().encode(res, encoding));
			receipt.setBusiness("GC");
			receipt.setAction("");

		} catch (Exception e1) {
			logger.error(e);
		}
		return receipt;
	}

	private Interface fillStoreCode(Interface request) {
		if (this.getStoreCode() == null)
			return request;
		if (request.getTransaction() == null)
			return request;
		String oldStoreCode = request.getTransaction().getStoreId();
		// if(oldStoreCode == null || oldStoreCode.equals(""))
		request.getTransaction().setStoreId(this.getStoreCode());
		logger.debug("Set store code(" + oldStoreCode + ") to " + request.getTransaction().getStoreId());
		return request;
	}

	private Interface fillBrandCode(Interface request) {
		if (this.getBrandCode() == null)
			return request;
		if (request.getTransaction() == null)
			return request;
		String oldBrandCode = request.getTransaction().getBrandCode();
		request.getTransaction().setBrandCode(this.getBrandCode());
		logger.debug("Set store code(" + oldBrandCode + ") to " + request.getTransaction().getBrandCode());
		return request;
	}

	private Interface fillPage(Interface request) {
		if (request.getTransaction() != null)
			return request;
		Page page = request.getPage();
		if (null == page) {
			page = new Page();
			page.setPageCurrent(1L);
			page.setPageSize(10L);
		}
		long oldPageCurrent = page.getPageCurrent();
		long oldPageSize = page.getPageSize();
		page.setPageCurrent(getPageCurrent());
		page.setPageSize(getPageSize());
		request.setPage(page);
		logger.debug("Set page current(" + oldPageCurrent + ") to " + page.getPageCurrent() + " ; " + "Set page size(" + oldPageSize + ") to "
				+ page.getPageSize());
		return request;
	}

	public String getStoreCode() {
		return bf.getStoreCode();
	}

	public void setStoreCode(String storeCode) {
		bf.setStoreCode(storeCode);
	}

	public String getBrandCode() {
		return bf.getBrandCode();
	}

	public void setBrandCode(String brandCode) {
		bf.setBrandCode(brandCode);
	}

	public long getPageSize() {
		return bf.getPageSize();
	}

	public void setPageSize(long pageSize) {
		bf.setPageSize(pageSize);
	}

	public long getPageCurrent() {
		return bf.getPageCurrent();
	}

	public void setPageCurrent(long pageCurrent) {
		bf.setPageCurrent(pageCurrent);
	}

}
