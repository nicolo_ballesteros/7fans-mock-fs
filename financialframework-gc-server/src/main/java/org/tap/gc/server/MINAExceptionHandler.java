package org.tap.gc.server;

import org.tap.communication.ISender;
import org.tap.communication.Receipt;
import org.tap.communication.mina.ExceptionOperation;
import org.tap.gc.ErrorCode;
import org.tap.gc.XmlHelper;
import org.tap.gc.security.ISecurity;
import org.tap.gc.security.support.NonSecurity;

public class MINAExceptionHandler extends ExceptionOperation {

	private String encoding = "UTF-8";
	
	private ISecurity security;

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public ISecurity getSecurity() {
		if(null == security)
			security = new NonSecurity();
		return security;
	}

	public void setSecurity(ISecurity security) {
		this.security = security;
	}

	private String encodeContent(String original) throws Exception  {
		return getSecurity().encode(original, encoding);
	}
	
	private Receipt getErrorResponse(String errorCode) throws Exception{
		return new Receipt(null, encodeContent(XmlHelper.marshaller(ErrorCode.getErrorResponse(errorCode))));		
	}

	private void handleException(ISender sender, Throwable t) {
		Exception e = (Exception) t;
		if(e.getMessage() == null){
			try {
				sender.operate(
						getErrorResponse(ErrorCode.CODE_SYSTEMERROR));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			return ;
		}
		if (e.getMessage().contains("javax.xml.bind.UnmarshalException")) {
			try {
				sender.operate(
						getErrorResponse(ErrorCode.CODE_UNMARSHALLERERROR));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			return;
		}

		if (e.getMessage().contains("javax.xml.bind.MarshalException")) {
			try {
				sender.operate(
						getErrorResponse(ErrorCode.CODE_MARSHALLERERROR));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			return;
		}

		if (e.getMessage().contains("org.apache.axis2.AxisFault")
				|| e.getMessage().contains("java.net.ConnectException") || e.getMessage().contains("java.net.SocketException")) {
			try {
				sender.operate(
						getErrorResponse(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		if(e.getMessage().contains("java.net.NoRouteToHostException")){
			sender.operate(ErrorCode.getErrorResponse(ErrorCode.CODE_NETWORKCABLEERROR));
			return ;
		}

		try {
			sender.operate(
					getErrorResponse(ErrorCode.CODE_SYSTEMERROR));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public Object exceptionHandler(ISender sender, Throwable cause) {
		handleException(sender, cause);
		return null;
	}

}
