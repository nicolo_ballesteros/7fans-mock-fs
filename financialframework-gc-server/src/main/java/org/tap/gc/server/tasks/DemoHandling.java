package org.tap.gc.server.tasks;

import com.tap.operation.OperationBase;

public class DemoHandling extends OperationBase {

	@Override
	public Object operate(Object info) {
		
		System.out.println("info's type: " + info.getClass());
		return null;
	}

}
