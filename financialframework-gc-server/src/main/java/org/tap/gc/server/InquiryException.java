package org.tap.gc.server;

public class InquiryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6413970493891864064L;

	public InquiryException(String errMsg){
		super(errMsg);
	}
	
	public InquiryException(Exception e){
		super(e);
	}
	
	public InquiryException(String errMsg, Exception e){
		super(errMsg, e);
	}
}
