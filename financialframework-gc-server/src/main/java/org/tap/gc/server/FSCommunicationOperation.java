package org.tap.gc.server;

import java.util.Properties;

import com.tap.operation.OperationBase;

public abstract class FSCommunicationOperation extends OperationBase {	
	protected String protocol;
	
	protected String host;
	
	protected int port;
	
	protected int timeout;
	
	protected Properties properties;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
