package org.tap.gc.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.tap.common.execsql.Constants;
import org.tap.gc.Converter;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Transaction;


/**
 * 
 * @author Jacky.LIU
 *
 */
public class TransactionConvert {
	public static final String TENDER_TYPE = "GC";
	
	public static final String TXNTYPE_SELLCOUPON = "Coupon Active";
	public static final String TXNTYPE_REDEEMCOUPON = "Coupon Redemption";
	
	public static final String ACTION_NAME_SaveTranactionRecord = "SaveTranactionRecord" ;
	
	  
//	OfflineStr,Confirm
//	OpCode,BusinessType,ErrorMessage,
//	SKU,TenderID,TxnType,BrandCode
//	StoreID,ServerID,PosID,CashierID,TxnNo,SN,TxnDate,TotalAmount,Amount,Balance,ItemUID,ItemUnitAmount,ItemCount,
//	ExpiryDate,ApprovalCode,CouponStatus,CouponAmount,CouponDeductAmount,ForfeitAmount,PrivateData
	public static JSONObject toJSONObjectData(Interface data){
		Transaction tr = data.getTransaction() ;
		JSONObject object = new JSONObject();
		try {
			object.put(Constants.CONFIG_Action,ACTION_NAME_SaveTranactionRecord) ;
			if(tr != null){
//				object.put("OfflineStr", "0");
				object.put("OfflineStr", tr.isOffline() == true ? "1" : "0" );
				object.put("SKU", tr.getSKU()) ; 
				object.put("TenderID", tr.getTenderId()) ; 
				object.put("TxnType", tr.getTxnType()) ;
				object.put("BrandCode", tr.getBrandCode());
				object.put("StoreID", tr.getStoreId());
				object.put("ServerID", tr.getServerId());
				object.put("POSID", tr.getPosId());
				object.put("CashierID", tr.getCashierId());
				object.put("TxnNo", tr.getTxnNo());
				object.put("SN", tr.getSN());
				object.put("TxnDate", tr.getTxnDate());
				
				String totalAmount = "0" ;
				Long totalAmountL =  tr.getTotalAmount() ;
				if(totalAmountL != null ){
					totalAmount = Converter.getAmountFromLongValue(totalAmountL) ;
				}
				object.put("TotalAmount", totalAmount);
				
				String balance = "0" ;
				Long balanceL =  tr.getBalance() ;
				if(balanceL != null ){
					balance = Converter.getAmountFromLongValue(balanceL) ;
				}
				object.put("Balance", balance);
				
				object.put("ItemUID", tr.getItemUID());
				object.put("MobileNumber", tr.getMobileNumber());
				
				String itemUnitAmount = "0" ;
				Long itemUnitAmountL =  tr.getItemUnitAmount();
				if(itemUnitAmountL != null ){
					itemUnitAmount = Converter.getAmountFromLongValue(itemUnitAmountL) ;
				}
				object.put("ItemUnitAmount", itemUnitAmount);
				object.put("ItemCount", tr.getItemCount());
				object.put("ExpiryDate", tr.getExpiryDate());
				object.put("ApprovalCode", tr.getApprovalCode());
				object.put("CouponStatus", tr.getCouponStatus());
				
				String couponAmount = "0" ;
				Long couponAmountL =  tr.getCouponAmount();
				if(couponAmountL != null ){
					couponAmount = Converter.getAmountFromLongValue(couponAmountL) ;
				}
				object.put("CouponAmount", couponAmount);
				
				String couponDeductAmount = "0" ;
				Long couponDeductAmountL =  tr.getCouponDeductAmount();
				if(couponDeductAmountL != null ){
					couponDeductAmount = Converter.getAmountFromLongValue(couponDeductAmountL) ;
				}
				object.put("CouponDeductAmount", couponDeductAmount);
				
				String forfeitAmount = "0" ;
				Long forfeitAmountL =  tr.getForfeitAmount();
				if(forfeitAmountL != null ){
					forfeitAmount = Converter.getAmountFromLongValue(forfeitAmountL) ;
				}
				object.put("ForfeitAmount", forfeitAmount);
				
				object.put("PrivateData", tr.getPrivateData());
				object.put("ProdCode", tr.getProdCode());
				object.put("ReferenceNo", tr.getReferenceNo());
				object.put("TransactionRRN", tr.getTransactionRRN()) ;
				object.put("Resubmit", "0") ;
				object.put("Lastupdatetime", getSystemTime()) ;
				
				// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate +++
				if(tr.getAuthDate() != null)
					object.put("AuthDate", tr.getAuthDate());
				if(tr.getConfirmDate() != null)
					object.put("ConfirmDate", tr.getConfirmDate());
				// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate ---
			}
			object.put("Confirm", "0" );
			object.put("OpCode", data.getOpCode());
			object.put("ErrorCode", data.getResponseCode());
			object.put("BusinessType", data.getBusinessType() );
			object.put("ErrorMessage", data.getErrorMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println(object.toString());
		return object ;
	}
	
	private static String getSystemTime(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = format.format(new Date()) ;
		return date ;
	}
	
	private Date getSystemTimebyDate(String time){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			return format.parse(time);
		} catch (ParseException e) {
		}
		return new Date() ;
	}
	
	public static Map<String, Object> toConfirmedSetsKeys(Interface buff){
		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("SN", data.getTargetSN());
		String responscode = buff.getResponseCode() ;
		int OpCode = buff.getOpCode() ;
		
		String resubmit = buff.getResubmit();
		
		if(resubmit == null || resubmit.equals("") || resubmit.equals("0")){
			map.put("Resubmit",  "0");
		}else if(resubmit.equals("1")){
			map.put("Resubmit",  "1");
		}
		
		map.put("OpCode", ""+ OpCode);
		if(responscode != null){
			map.put("ErrorCode", responscode);
			if(responscode.equals("0")  || responscode.equals("00")){
				map.put("Confirm", "1");
			}
		}else{
			map.put("Confirm", "1");
		}
		String errorMessage = buff.getErrorMessage() ;
		if(errorMessage != null){
			map.put("ErrorMessage", errorMessage);
		}
		
		String lastupdatetime = buff.getLastupdatetime();
		if(lastupdatetime != null){
			map.put("Lastupdatetime", lastupdatetime);
		}
		
		Transaction  tr = buff.getTransaction();
		if(tr != null){
			String prodCode = tr.getProdCode() ;
			String referenceNo = tr.getReferenceNo();
			String transactionRRN = tr.getTransactionRRN();
			if(prodCode != null){
				map.put("ProdCode", prodCode);
			}
			if(referenceNo != null){
				map.put("ReferenceNo", referenceNo);			
			}
			if(transactionRRN != null){
				map.put("TransactionRRN", transactionRRN);
			}
			// By Ivan.WONG 2016-09-08: add ConfirmDate
			map.put("ConfirmDate", (tr.getConfirmDate()==null) ? "" : tr.getConfirmDate());
		}
		return map;
	}
	
	public static Map<String, Object> toConfirmedReferKeys(Interface buff){
		Transaction data = buff.getTransaction();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("SN", data.getTargetSN());
		return map;
	}
	
	public static void main(String[] args) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = format.format(new Date()) ;
		
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			Date ta = format2.parse(date);
			System.out.println(ta);
		} catch (ParseException e) {
		}
			
	}
}
