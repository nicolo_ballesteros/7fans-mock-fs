package org.tap.gc.server;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.tap.communication.IReadLogic;
import org.tap.communication.IWriteLogic;
import org.tap.communication.tcp.TCPAccess;
import org.tap.gc.security.ISecurity;
import org.tap.gc.security.support.NonSecurity;

import com.tap.operation.OperationException;

/**
 * <p>
 * Wrapper {@link TCPAccess} and controlled socket in Method
 * {@link TCPAccessOperation#operate(String)}
 * 
 * <p>
 * It can control socket connection time, that you need set a property into
 * {@link FSCommunicationOperation#properties}, "longConnection" you need to set
 * true, and "activeTime" would be a active time, it would affect connection
 * time. if activeTime is -1, it means that connection is always active.
 * 
 * <p>
 * Keep every request to a short connection, just set "longConnection" to false
 * or don't add "longConnection" property.
 * 
 * <p>
 * messageEncoding is set message encoding
 * 
 * <p>
 * retryAfterConnectionRefused set retry count after socket lose connection
 * 
 * @author Jacky.LIU
 * 
 */
public class TCPAccessOperation extends FSCommunicationOperation {
	protected final Logger logger = Logger.getLogger(this.getClass());

	private TCPAccess tcpAccess;

	private boolean longConnection;

	private long activeTime = -1;

	/**
	 * It will use schedule to monitor active connection, and invoke task, it
	 * will disconnect connection.
	 */
	private Timer activeTimer;

	private final ReentrantLock lock = new ReentrantLock();

	private final IReadLogic readLogic;      

	private final IWriteLogic writeLogic;

	private int retryAfterConnectionRefused = 0;

	private ISecurity security;
	
	private boolean closed4LongConnection = false;

	public TCPAccessOperation(IReadLogic readLogic, IWriteLogic writeLogic) {
		this.readLogic = readLogic;
		this.writeLogic = writeLogic;
	}

	@Override
	public Object operate(Object info) {
		final ReentrantLock runLock = this.lock;
		runLock.lock();
		String receipt = null;
		try {
			connect();
			// access need to be used DES algriothm to encode and decode
			receipt = getSecurity().decode(
					tcpAccess.request(
							getSecurity().encode((String) info,
									tcpAccess.getMessageEncoding()),
							writeLogic, readLogic),
					tcpAccess.getMessageEncoding());
			return receipt;
		} catch(SocketTimeoutException e){
			logger.error(e.getMessage(), e);
			try {
				enforceDisconnect();
			} catch (IOException e1) {
				logger.error(e.getMessage(), e);
			}
			throw new OperationException(e);
		}catch (ConnectException e) {
			logger.error(e.getMessage(), e);
			try {
				return operateException(info, e);
			} catch (Exception e1) {
				throw new OperationException(e1.getMessage());
			}
		} catch (IOException e){
			logger.error(e.getMessage(), e);
			try {
				return operateException(info, e);
			} catch (Exception e1) {
				throw new OperationException(e1);
			}
		}catch (Exception e) {
			throw new OperationException(e);
		} finally {

			try {
				disconnect();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			runLock.unlock();
		}
	}
	
	private String operateException(Object info, Exception e) throws Exception{
		String receipt = null;
		try {
			closed4LongConnection = true;
			disconnect();
		} catch (IOException e1) {
			logger.error(e1.getMessage(), e1);
		}
		boolean result = retryConnect();
		if (!result)
			throw new OperationException(e);

		try {
			receipt = getSecurity().decode(
					tcpAccess.request(
							getSecurity().encode((String) info,
									tcpAccess.getMessageEncoding()),
							writeLogic, readLogic),
					tcpAccess.getMessageEncoding());
		} catch (IOException e1) {
			throw new OperationException(e1);
		}
		return receipt;
	}

	public ISecurity getSecurity() {
		if (null == security)
			security = new NonSecurity();
		return security;
	}

	public void setSecurity(ISecurity security) {
		this.security = security;
	}

	private boolean retryConnect() {
		logger.debug("It's ready to retry to connect, count is "
				+ this.retryAfterConnectionRefused);
		for (int i = 0; i < this.retryAfterConnectionRefused; i++) {
			try {
				connect();
				return true;
			} catch (UnknownHostException e) {
				logger.warn(e.getMessage());
			} catch (IOException e) {
				logger.warn(e.getMessage());
			}
			try {
				disconnect();
			} catch (IOException e) {
				logger.warn(e.getMessage());
			}
		}
		return false;
	}

	public TCPAccess getTCPAccess() {
		if (null == tcpAccess) {
			tcpAccess = new TCPAccess(super.getHost(), super.getPort());
			initTCPAccess(tcpAccess);
		}
		return this.tcpAccess;
		
 
	}

	public int getRetryAfterConnectionRefused() {
		return retryAfterConnectionRefused;
	}

	public void setRetryAfterConnectionRefused(int retryAfterConnectionRefused) {
		this.retryAfterConnectionRefused = retryAfterConnectionRefused;
	}

	private void initTCPAccess(TCPAccess tcpAccess) {
		tcpAccess.setSoTimeout(super.getTimeout());
		logger.debug("set soTimeout is " + tcpAccess.getSoTimeout());
		Properties props = super.getProperties();
		if (null == props)
			return;
		if (props.containsKey("longConnection")
				&& "true".equals(props.getProperty("longConnection")
						.toLowerCase())) {
			longConnection = true;
			logger.debug("enable longConnection");
		}
		if (longConnection && props.containsKey("activeTime")) {
			activeTime = Long.parseLong(props.getProperty("activeTime"));
			logger.debug("set active time is " + activeTime);
		}
		if (props.containsKey("messageEncoding")) {
			tcpAccess.setMessageEncoding(props.getProperty("messageEncoding"));
			logger.debug("set messageEncoding is "
					+ tcpAccess.getMessageEncoding());
		}
	}

	private void connect() throws UnknownHostException, IOException {
		TCPAccess tcpAccess = getTCPAccess();
		if (!tcpAccess.isClosed())
			return;
		tcpAccess.connect();
		closed4LongConnection = false;
		if (!longConnection)
			return;
		if (null != activeTimer) {
			activeTimer.cancel();
			activeTimer.purge();
		}
		if (null == activeTimer) {
			activeTimer = new Timer();
		}

		if (activeTime == -1) {
			logger.debug("Connection would be never disconnected.");
			return;
		}
		Date disconnectingTime = new Date(System.currentTimeMillis()
				+ activeTime);
		activeTimer.schedule(new MonitorActiveConnection(), disconnectingTime);
		logger.debug("Disconnecting socket schedule would be invoke at "
				+ disconnectingTime);
	}

	private void disconnect() throws IOException {
		TCPAccess tcpAccess = getTCPAccess();
		if (tcpAccess.isClosed())
			return;
		if (longConnection && !closed4LongConnection){
			return;
		}
		tcpAccess.disconnect();
	}
	
	/**
	 * Whatever long connection or short connection, it will enfore to close connection.
	 * @throws IOException 
	 */
	private void enforceDisconnect() throws IOException{
		TCPAccess tcpAccess = getTCPAccess();
		if(tcpAccess.isClosed())
			return ;
		tcpAccess.disconnect();
	}

	final class MonitorActiveConnection extends TimerTask {

		@Override
		public void run() {
			final ReentrantLock runLock = lock;
			runLock.lock();
			logger.debug("Active time monitor thread begin to run.");
			try {
				disconnect();
				logger.debug("Disconnect socket");
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			} finally {
				runLock.unlock();
			}
		}

	}

}
