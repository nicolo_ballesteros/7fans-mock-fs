package org.tap.gc.server;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.tap.communication.IPacket;
import org.tap.communication.tcp.IReceiverExceptionHandler;
import org.tap.gc.ErrorCode;

public class CFSExceptionHandler implements IReceiverExceptionHandler {
	private final Logger logger = Logger.getLogger(this.getClass());

	@Override
	public void handleException(IPacket packet, Throwable t) {
		Exception e = (Exception)t;
		logger.error("handleException:" + e.getMessage(), e);
		if(e.getMessage() == null){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_SYSTEMERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		if(e.getMessage().contains("javax.xml.bind.UnmarshalException")){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_UNMARSHALLERERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		if(e.getMessage().contains("javax.xml.bind.MarshalException")){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_MARSHALLERERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		if(e.getMessage().contains("Read timed out") || e.getMessage().contains("Executed timeout")){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_READTIMEOUTERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		if(e.getMessage().contains("java.net.ConnectException") || e.getMessage().contains("java.net.SocketException")){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_CONNECTTOFSERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		if(e.getMessage().contains("java.net.NoRouteToHostException")){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_NETWORKCABLEERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
			return ;
		}
		
		try {
			packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_SYSTEMERROR));
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
	}

}
