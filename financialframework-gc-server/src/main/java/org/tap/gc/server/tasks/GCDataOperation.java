package org.tap.gc.server.tasks;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.tap.gc.OpCodeConstants;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.BeepResponse;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Transaction;
import org.tap.gc.json.JSONHelper;
import org.tap.gc.server.BufferData;
import org.tap.gc.server.TransactionConvert;
import org.tap.server.datasource.EFTPaymentDataException;
import org.tap.server.datasource.IVerificationPaymentData;
import org.tap.server.datasource.SaveEFTPaymentDataOperation;
import org.tap.server.datasource.beep.BeepSaveOperation;
import org.tap.server.datasource.billspayment.BillsSaveOperation;

import com.tap.operation.OperationBase;
import com.tap.operation.OperationException;

/**
 * Handling Interface's Transaction data
 * 
 * @author Jacky.LIU
 * 
 */

//FIXME !!! [1293DB] GCDATA  - FS
public class GCDataOperation extends OperationBase {
	private final Logger logger = Logger.getLogger(this.getClass());

	private final SaveEFTPaymentDataOperation saveOperation;

	private final IVerificationPaymentData verification;

	public GCDataOperation() {
		saveOperation = new SaveEFTPaymentDataOperation();
		this.verification = new IVerificationPaymentData() {

			@Override
			public void verificationData(JSONObject data)
					throws EFTPaymentDataException {}
		};
		saveOperation.setVerification(verification);
	}

	@Override
	public Object operate(Object info) {

		Interface buff = null;
		try {
			if (info instanceof String) {
				String data = (String) info;
				logger.debug("Handling data: " + data);
				try {
					buff = (Interface) XmlHelper.unmarshaller(data);
				} catch (JAXBException e) {
					logger.error(e.getMessage());
					throw new OperationException(e);
				}
			} else {
				buff = (Interface) info;
				logger.debug("Handling data: SN:" + ((buff.getTransaction() == null)?"null":buff.getTransaction().getSN()) + ", TxnType:" + 
						((buff.getTransaction() == null)?"null":buff.getTransaction().getTxnType()));
			}

			if (null == buff.getTransaction() && null == buff.getRecallTxnNo())
				throw new OperationException(
						"Interface didn't contain transaction or recalltxnno object.");
			
//			if(buff.getRecallTxnNo() != null){
//				logger.debug("Recall old txnno:" + buff.getRecallTxnNo().getOldTxnNo() 
//						+ ", new txnno:" + buff.getRecallTxnNo().getNewTxnNo());
//				int affectedRows = saveOperation.recall(buff.getRecallTxnNo().getOldTxnNo(), buff.getRecallTxnNo().getNewTxnNo());
//				logger.debug("Affected rows: " + affectedRows);
//				return buff;
//			}
				
			
				String busType = buff.getBusinessType();
				
				logger.debug("Business Type: "+busType);
				//TODO 1360 - DB Saving - FS
				if ((busType!=null && busType.equals(OpCodeConstants.BUSINESSTYPE_BEEP_PAYMENT))
						||(busType!=null && busType.equals(OpCodeConstants.BUSINESSTYPE_BEEP_TOPUP))){
					
					logger.debug("Saving BEEP Transaction: "+XmlHelper.marshaller(buff));
					
					Transaction txn = buff.getTransaction();
					BeepResponse bResp = buff.getBeepResponse();
					String txnType = null;
				

					
					if(txn!=null){
						txnType=txn.getTxnType();
					}
					
					if (txnType!=null && txnType.equals("Confirm")){
						BeepSaveOperation bso = new BeepSaveOperation();
						bso.confirmBeepPayment(buff);
					}else if(bResp!=null){
						BeepSaveOperation bso = new BeepSaveOperation();
						bso.updateBeepPayment(buff);
					}else{						
						BeepSaveOperation bso = new BeepSaveOperation();
						bso.saveBeepPayment(buff);						
					}
					
					
				
				}else if (busType!=null && busType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER)) {
					
					try{
						logger.debug("XML : " + XmlHelper.marshaller(buff));
						Transaction txn = buff.getTransaction();
						String txnType = null;
						String subBusinessType = txn.getSubBusinessType();

						if(txn!=null){
							txnType=txn.getTxnType();
						}
						
						if (txnType!=null && txnType.equals("Confirm")){
							BillsSaveOperation update = new BillsSaveOperation();							
							String sN = buff.getTransaction().getSN();
							logger.debug("Updating Confirm Status of Transaction SN: "+sN);
							
							Integer affectedRows = update.updateBillsPayment(sN);
							logger.debug("Updated rows: " + affectedRows + " for SN: "+sN);
							return buff;
							
						}else{
							BillsSaveOperation bso = new BillsSaveOperation();
							String sN = buff.getTransaction().getSN();
							boolean isRecordExisting = bso.checkIfRecordExists(sN);
							logger.debug("Record Exists in Bills Payment Data: "+isRecordExisting);
							if(!isRecordExisting){
								
								boolean status = false;													
								status = bso.saveBillsPayment(buff, subBusinessType);
								logger.debug("Saving data status=" + status);
								if(status){
									logger.debug("Data Saved Successfully");
									
									boolean isInputRecordExisting = bso.checkIfInputRecordExists(sN);
									logger.debug("Record Exists in Bills Payment Input Data: "+isInputRecordExisting);
									if(!isInputRecordExisting){
										bso.saveBillsPaymentInput(buff);
									}
									logger.debug("Input Data Saved Successfully");
									
									
									return buff;
								}else{
									handleFailureOperation("Save to database failure, SN:"
											+ buff.getTransaction().getSN(), buff);
								}
							}else{
								
								logger.debug("Updating Recorded Transaction SN: "+sN);
								
								Integer affectedRows = bso.replaceBillsPayment(buff, subBusinessType);
								logger.debug("Updated rows: " + affectedRows + " for SN: "+sN);
								
								if(affectedRows>0){
									logger.debug("Data Updated Successfully");
									
									boolean isInputRecordExisting = bso.checkIfInputRecordExists(sN);
									logger.debug("Record Exists in Bills Payment Input Data: "+isInputRecordExisting);
									if(!isInputRecordExisting){
										bso.saveBillsPaymentInput(buff);
									}
									logger.debug("Input Data Saved Successfully");
									
									return buff;
								}else{
									handleFailureOperation("Save to database failure, SN:"
											+ buff.getTransaction().getSN(), buff);
								}								
							}
							
							
						}
												

						
					}catch(Exception e){						
						logger.error("Error in Saving/Updating Bills Payment Transaction.");
						handleFailureOperation("Save to database failure, SN:"
								+ buff.getTransaction().getSN(), buff);
					}
				
										
				}else{
					
					if ("Confirm".equals(buff.getTransaction().getTxnType())) {
						String apcode =  buff.getTransaction().getApprovalCode();
						if(apcode == null){
							apcode = "" ;
						}
						logger.debug("Confirming SN no: "
								+ buff.getTransaction().getSN() + " transactions.");
						int affectedRows = saveOperation.confirm(TransactionConvert.toConfirmedSetsKeys(buff) ,
								TransactionConvert.toConfirmedReferKeys(buff),apcode);
						logger.debug("Affected rows: " + affectedRows);
						return buff;
					}
					
					//added if else above
					JSONObject requestData = TransactionConvert.toJSONObjectData(buff);
					logger.debug("requestData=" + requestData);
					boolean status = (Boolean) saveOperation.operate(requestData);
					logger.debug("Saving data status=" + status);
					if (status) {
						logger.debug("Saving data successfully.");
						return buff;
					}

					handleFailureOperation("Save to database failure, SN:"
							+ buff.getTransaction().getSN(), buff);
					
					
				}
		


		
			

		} catch (Exception e) {
			e.printStackTrace();
			handleFailureOperation(e.getMessage(), buff);
			throw new OperationException(e);
		}
		return buff;
	}

	private void handleFailureOperation(String message, Interface buff) {
		logger.debug(message
				+ ", please check datatrace.log for more information");
		if (null != buff)
			try {
				Logger.getLogger("DataTrace").trace(
						"failure data:" + XmlHelper.marshaller(buff));
			} catch (JAXBException e) {
				Logger.getLogger("DataTrace").error(e.getMessage());
			}
	}

	private Interface filterForOfflineTransaction(Interface data) {
		if (!data.getTransaction().isOffline())
			return data;

		if (data.getTransaction().getCouponDeductAmount() == null
				|| data.getTransaction().getCouponDeductAmount() == 0) {
			data.getTransaction().setCouponDeductAmount(
					data.getTransaction().getAmount());
			logger.debug("set coupon actual amount to be "
					+ data.getTransaction().getCouponDeductAmount());
		}
		String settingStoreCode = BufferData.getInstance().getStoreCode();
		if(null != settingStoreCode){
			data.getTransaction().setStoreId(settingStoreCode);
			logger.debug("set storecode to be "
					+ data.getTransaction().getStoreId());
		}
		return data;
	}

	private Interface filterForOnlineTransaction(Interface data) {
		if (data.getTransaction().isOffline())
			return data;

		if (!data.getTransaction().getTxnType().equals(JSONHelper.TXNTYPE_SELLCOUPON))
			return data;
		
		logger.debug("filter for online selling transaction.");
		if (data.getTransaction().getCouponDeductAmount() == null
				|| data.getTransaction().getCouponDeductAmount() == 0) {
			data.getTransaction().setCouponDeductAmount(
					data.getTransaction().getAmount());
			logger.debug("set coupon actual amount to be "
					+ data.getTransaction().getCouponDeductAmount());
		}
		return data;
	}
	
	public static void main(String[] args) {
		GCDataOperation o = new GCDataOperation() ;
//		String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Interface OpCode=\"100\" ResponseCode=\"0\" ErrorMessage=\"\" businessType=\"TelecoServer\" xmlns=\"http://www.tap.org/gc/beans\"><Transaction StoreId=\"00101\" PosId=\"5\" TxnDate=\"2015-06-18 12:00:00\" TxnNo=\"0010100000520150618000593\" SN=\"0010100000520150618000593007173\" TargetSN=\"0010100000520150618000593007171\" Amount=\"1000\" TotalAmount=\"1000\" ItemUID=\"WSOTH10\" TransactionRRN=\"520132008459\" MobileNumber=\"09335174191\" CardNumber=\"333300000000002\" CouponUID=\"WSOTH10\" CashierId=\"1\" ApprovalCode=\"219787\" TxnType=\"Confirm\" BrandCode=\"02\" CouponDeductAmount=\"1000\" SKU=\"00000000711959\"/></Interface>" ;
//		String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Interface OpCode=\"100\" businessType=\"GCServer\" xmlns=\"http://www.tap.org/gc/beans\"><Transaction StoreId=\"00101\" PosId=\"5\" TxnDate=\"2015-06-23 12:00:00\" TxnNo=\"0010100000520150623000656\" SN=\"0010100000520150623000656007579\" TargetSN=\"0010100000520150623000656007577\" Amount=\"10000\" TotalAmount=\"10000\" CashierId=\"1\" TxnType=\"Confirm\" BrandCode=\"02\" SKU=\"00000000051078\"/></Interface>" ;
//		String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Interface OpCode=\"100\" businessType=\"GCServer\" xmlns=\"http://www.tap.org/gc/beans\"><Transaction StoreId=\"00101\" PosId=\"5\" TxnDate=\"2015-06-23 12:00:00\" TxnNo=\"0010100000520150623000656\" SN=\"0010100000520150623000656007579\" TargetSN=\"0010100000520150623000656007577\" Amount=\"10000\" TotalAmount=\"10000\" CashierId=\"1\" TxnType=\"Confirm\" BrandCode=\"02\" SKU=\"00000000051078\" ConfirmDate=\"2016-12-31\"/></Interface>" ;
		String data="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Interface OpCode=\"101\" ResponseCode=\"90414\" ErrorMessage=\"System Error\" businessType=\"BillPaymentServer\" xmlns=\"http://www.tap.org/gc/beans\"><Transaction StoreId=\"00318\" PosId=\"11\" TxnDate=\"2017-12-15 12:00:00\" TxnNo=\"0031800001120171215000046\" SN=\"0031800001120171215000046010546\" TotalAmount=\"0\" ItemUnitAmount=\"0\" CashierId=\"2\" BrandCode=\"01\" SKU=\"00000000000020\" AuthDate=\"2017-12-15 10:32:23\"/><BillerInfoList><BillerInfo ItemCode=\"MRN\" InputValue=\"11111111112222222222333333\"/><BillerInfo ItemCode=\"PayFor\" InputValue=\"TCA\"/><BillerInfo ItemCode=\"AmountPaid\" InputValue=\"11\"/><BillerInfo ItemCode=\"OtherCharges\" InputValue=\"0\"/><BillerInfo ItemCode=\"biller_code\" InputValue=\"00001\"/><BillerInfo ItemCode=\"service_code\" InputValue=\"MECOA\"/></BillerInfoList></Interface>";
		System.out.println(data);
		o.operate(data);
	}
}
