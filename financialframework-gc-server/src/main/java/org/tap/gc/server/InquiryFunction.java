package org.tap.gc.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.tap.gc.beans.BillerInfo;
import org.tap.gc.beans.BillerInfoList;
import org.tap.gc.beans.Interface;
import org.tap.server.datasource.impl.ServerOperatingDB;

public class InquiryFunction{
private static final Logger logger = Logger.getLogger("InquiryFunction");
	
	public static boolean validateRetryFlag(Interface request) throws InquiryException{
		String accountNo = "";
		
		BillerInfoList biList = request.getBillerInfoList();
		
		if (biList == null) return true;
		
		List<BillerInfo> bInfo = biList.getBillerInfo();

		/*
		 * Get MRN / Account Number
		 */
		for (BillerInfo bi : bInfo){			
			if (bi.getItemCode().equals("MRN") || bi.getItemCode().equals("AccountNo")){
				accountNo = bi.getInputValue();
				break;
			}
		}
		
		if (accountNo.equals("")){
			logger.warn("No Account Number read from Biller Info List. Will assume invalid");
			throw new InquiryException("No Account Number read from Biller Info");
		}
		
		
		/*
		 * Get fields from database based on MRN / Account Number 
		 */
		String validationQuery = "select RetryFlag from BillsPaymentData bpd"
				+ " left join BillsPaymentInputData bpid on bpd.SN = bpid.SN"
				+ " where(bpid.ItemCode = 'MRN' or bpid.ItemCode = 'AccountNo') and bpid.InputValue = ?";
		
		String updateQuery = "update bpd set bpd.RetryFlag = 1 from BillsPaymentData bpd"
				+ " left join BillsPaymentInputData bpid on bpd.SN = bpid.SN where "
				+ " ((bpid.ItemCode = 'MRN' or bpid.ItemCode = 'AccountNo') and bpid.InputValue = ?)"
				+ " and bpd.RetryFlag = 0";

		
		
		ServerOperatingDB sodb = new ServerOperatingDB();
		logger.debug("Getting RetryFlag from DB");
		Connection dbConnection = null;
		PreparedStatement ps = null;
		PreparedStatement psUpdate = null;

		try{
			try{
				DataSource ds = sodb.getDataSource();
				dbConnection = ds.getConnection();
				
				ps = dbConnection.prepareStatement(validationQuery);
				ps.setString(1, accountNo);
				ResultSet rs = ps.executeQuery();
				
				int count = 0;
				while(rs.next()){
					if (rs.getString("RetryFlag") == null) {
						continue;
					}
					else if (!rs.getString("RetryFlag").equals("0")){
						count++;
						continue;
					}else {
						logger.info("Retry valid");
//						psUpdate = dbConnection.prepareStatement(updateQuery);
//						
//						psUpdate.setString(1, accountNo);
//						
//						int result = psUpdate.executeUpdate();
//						logger.info("Rows affected: " + result);
						return true;
					}
				}
				
				logger.debug("No valid transactions to retry");
				String err = count > 0 ? "Retry Invalid" : "No records found";
				throw new InquiryException(err);
			} catch (SQLException sqle){
				logger.error("Exception caught while getting fields in database", sqle);
				throw new InquiryException("Exception caught while getting fields in database", sqle);
			} finally{
				if (ps != null) {
					ps.close();
				}
				
				if (psUpdate != null){
					psUpdate.close();
				}
				
				if(dbConnection!=null){
					dbConnection.close();
				}
			}
		} catch(SQLException sqle){
			logger.error("Exception caught while closing connections", sqle);
			throw new InquiryException("Exception caught while closing connections",sqle);
		}	
		
	}
}
