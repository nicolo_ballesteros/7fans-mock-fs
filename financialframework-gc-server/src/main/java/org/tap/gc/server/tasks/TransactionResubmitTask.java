package org.tap.gc.server.tasks;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Transaction;
import org.tap.gc.json.JSONHelper;
import org.tap.svawebservicegateway.ISVAWebServiceGateway;

import com.tap.operation.IOperation;
import com.tap.operation.OperationBase;
import com.tap.operation.OperationException;

public class TransactionResubmitTask extends OperationBase implements Runnable{
	public IOperation getSerialization() {
		return serialization;
	}

	public void setSerialization(IOperation serialization) {
		this.serialization = serialization;
	}

	public static final String FILE_XML = "xml";
	
	public static final String FILE_SERIALIZATION = "ser";
	
	private IOperation serialization;
	
	private File path;
	
	private boolean recursive = false;
	
	private String fileNameExpression;
	
	private static  final Logger logger = Logger.getLogger(TransactionResubmitTask.class);
	
	private ISVAWebServiceGateway svaGateway  ;
	
	private QueueConnectionFactory queueConnectionFactory;
	
	private String queueName;
	
	public TransactionResubmitTask(ISVAWebServiceGateway svaGateway){
		this.svaGateway = svaGateway;
	}
	
	/**
	 * Parameter of info may be null, it will return a list of file of uploaded to JMS queue
	 */
	@Override
	public Object operate(Object info) {
		File[] files;
		if(null != info && (info instanceof File)){
			File f = (File)info;
			logger.debug("operate pass a type of File(" + f + "), it's ready to operate it.");
			files = listFiles(f);
		}else if(path == null){
			throw new NullPointerException(this.getClass().getName() + "'s path can't be null.");
		}else{
			logger.debug("operate pass a type of File(" + path + "), it's ready to operate it.");
			files = listFiles(path);
		}
		
		logSearchedFiles(files);
		try {
			resubmitConfirm(files);
		} catch (JMSException e) {
			logger.error("Operate JMSTask failure: " + e.getMessage());
			throw new OperationException(e);
		} catch (ClassNotFoundException e) {
			logger.error("Operate JMSTask failure: " + e.getMessage());
			throw new OperationException(e);
		}
		return files;
	}
	
	private void logSearchedFiles(File[] files){
		StringBuilder sb = new StringBuilder();
		sb.append("Searched files: ");
		sb.append("\r\n");
		
		for(File file : files){
			sb.append("\t\t");
			sb.append(file.toString());
			sb.append("\r\n");
		}
		
		logger.info(sb.toString());		
	}
	
	private File[] listFiles(File path){
		if(path.isFile())return new File[]{path};
		File[] rootFiles;
		if(fileNameExpression == null){
			rootFiles = path.listFiles();
		}else{
			rootFiles = path.listFiles(new ExpressionFilenameFilter(fileNameExpression));
		}

		if(!recursive)return rootFiles;
		List<File> files = new ArrayList<File>();
		for(File rootFile : rootFiles){
			files.add(rootFile);
		}
		
		File[] folders = path.listFiles(new FileFilter(){
	
				@Override
				public boolean accept(File pathname) {
					if(pathname.isDirectory())return true;
					return false;
				}
				
			});
		
		// loop current path's all files of sub-folder
		for(File folder : folders){
			File[] subFiles = listFiles(folder);
			if(subFiles.length == 0)continue;
			
			for(File subFile : subFiles){
				files.add(subFile);
			}
		}
		
		return files.toArray(new File[files.size()]);
	}
	
	private void resubmitConfirm(File[] files) throws JMSException, ClassNotFoundException{
		if(files.length == 0){
			logger.info("Uploaded file size is zero, return uploadToJMSQueue method.");
			return ;
		}
		Connection connection = null;
		try{
			logger.debug("It's ready to connect to jms service");
			connection = queueConnectionFactory.createConnection();
			logger.debug("Connected successfully: " + connection);
			Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(queueName);
			MessageProducer producer = session.createProducer(queue);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);
			
			logger.info("Created  MessageProducer:" + producer.toString() + " successfully.");
			for(File file : files){
				try {
//					Interface response = new Interface();
//					response.setResponseCode("0");
					Interface response = resubmitConfirm(file);
					if(response != null){
						String responsecode = response.getResponseCode();
						if(responsecode != null && responsecode.equals("0")){
							response.setResponseCode(null);
							response.setResubmit("1");
							response.setLastupdatetime(this.getSystemTime());
//							processEFTPaymentData(response);
							
							uploadToJMSQueue(response, session, producer);
						}else{
							logger.info("resubmit transaction failure. Lost trading data. ");
						}	
						boolean status = file.delete();
						if(status)
							logger.info("Deleted file(" + file + ") successfully.");
						else
							logger.warn("Deleted file(" + file + ") failure.");
					}
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
//				} catch (JAXBException e) {
//					logger.error(e.getMessage(), e);
//				} catch (JSONException e) {
//					logger.error(e.getMessage(), e);
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}finally{
			if(connection != null)
				connection.close();
			logger.info("Disconnected " + connection + ":queue:" + queueName + " successfully.");
		}
	}
	
	private String getSystemTime(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = format.format(new Date()) ;
		return date ;
	}
	
	private Interface resubmitConfirm(File file) throws JAXBException, JSONException, RemoteException {
		Interface info = createMessage(file);	
		if(info.getResponseCode() != null){
			info.setResponseCode(null);
		}
		JSONObject request = JSONHelper.toJSONObject(info);
		if(request.has("ResponseCode")){
			request.remove("ResponseCode");
			logger.debug("remove responseCode");
		}
		JSONObject response = requestToSVA(request);
		return fillTransaction(info, JSONHelper.toInterface(response));
	}
	
	private Interface createMessage(File file) throws JAXBException{
		Interface message = (Interface) XmlHelper.unmarshaller(file) ;
		return message;
	}
	
private JSONObject requestToSVA(JSONObject request) throws RemoteException, JSONException{
	logger.debug("Sent request: " + request);
	JSONObject response = svaGateway.request(request);
	logger.debug("Received response: " + response);
	return response;	
}
	
	private Interface fillTransaction(Interface request, Interface response){
		response.setOpCode(request.getOpCode());
		response.setBusinessType(request.getBusinessType());
		//text
//		response.setResponseCode("0") ;
		//te
		if(null != request.getRecallTxnNo()){
			response.setRecallTxnNo(request.getRecallTxnNo());
			return response;
		}
		if(response.getCoupon() != null && request.getCoupon() != null){
			if(response.getCoupon().getCouponUID() == null){
				response.getCoupon().setCouponUID(request.getCoupon().getCouponUID());
			}
		}
		if(request.getTransaction() == null)return response;
		if(!"0".equals(response.getResponseCode()))return response;
		Transaction requestTxn = request.getTransaction();
		Transaction responseTxn = response.getTransaction();
		if(responseTxn == null)return response;
		
		if(responseTxn.getApprovalCode() == null){
			responseTxn.setApprovalCode(requestTxn.getApprovalCode());
		}
		if(responseTxn.getTotalAmount() == null){
			responseTxn.setTotalAmount(requestTxn.getTotalAmount());
//			responseTxn.setTotalAmount(requestTxn.getTotalAmount());
		}
		if(responseTxn.getItemUnitAmount() == null){
			responseTxn.setItemUnitAmount(requestTxn.getItemUnitAmount());
		}
		if(responseTxn.getItemCount() == null){
			responseTxn.setItemCount(requestTxn.getItemCount());
		}
		if(responseTxn.getItemUID() == null){
			String uid = requestTxn.getItemUID();
			responseTxn.setItemUID(uid);
		}
		
		if(responseTxn.getMobileNumber() == null){
			String mobileNumber = requestTxn.getMobileNumber();
			responseTxn.setMobileNumber(mobileNumber);
		}
		
		if(responseTxn.getSKU() == null){
			responseTxn.setSKU(requestTxn.getSKU());
		}
		
		if(responseTxn.getTenderId() == null){
			responseTxn.setTenderId(requestTxn.getTenderId());
		}
		
		if(requestTxn.getTxnType() != null){
			responseTxn.setTxnType(requestTxn.getTxnType());
		}
		
//		if(responseTxn.getTxnType() == null){
//			responseTxn.setTxnType(requestTxn.getTxnType());
//		}
		
		if(responseTxn.getTargetSN() == null){
			responseTxn.setTargetSN(requestTxn.getTargetSN());
		}
		
		if(responseTxn.getBalance() == null){
			responseTxn.setBalance(requestTxn.getBalance());
		}
		if(responseTxn.getAmount() == null){
			responseTxn.setAmount(requestTxn.getAmount());
		}
		
		if(responseTxn.getMobileNumber() == null){
			responseTxn.setMobileNumber(requestTxn.getMobileNumber());
		}
		if(responseTxn.getForfeitAmount() == null){
			responseTxn.setForfeitAmount(requestTxn.getForfeitAmount());
		}
		if(responseTxn.getCouponAmount() == null){
			responseTxn.setCouponAmount(requestTxn.getCouponAmount());
		}
		if(responseTxn.getBusDate() == null){
			responseTxn.setBusDate(requestTxn.getBusDate());
		}
		if(responseTxn.getCardNumber() == null){
			responseTxn.setCardNumber(requestTxn.getCardNumber());
		}
		if(responseTxn.getCardStatus() == null){
			responseTxn.setCardStatus(requestTxn.getCardStatus());
		}
		if(responseTxn.getCardType() == null){
			responseTxn.setCardType(requestTxn.getCardType());
		}
		if(responseTxn.getCashierId() == null){
			responseTxn.setCashierId(requestTxn.getCashierId());
		}
		if(responseTxn.getCouponCount() == null){
			responseTxn.setCouponCount(requestTxn.getCouponCount());
		}
		if(responseTxn.getCouponStatus() == null){
			responseTxn.setCouponStatus(requestTxn.getCouponStatus());
		}
		if(responseTxn.getCouponUID() == null){
			responseTxn.setCouponUID(requestTxn.getCouponUID());
		}
		if(responseTxn.getDescription() == null){
			responseTxn.setDescription(requestTxn.getDescription());
		}
		if(responseTxn.getExpiryDate() == null){
			responseTxn.setExpiryDate(requestTxn.getExpiryDate());
		}
		if(responseTxn.getPosId() == null){
			responseTxn.setPosId(requestTxn.getPosId());
		}
		if(responseTxn.getServerId() == null){
			responseTxn.setServerId(requestTxn.getServerId());
		}
		if(responseTxn.getSN() == null){
			responseTxn.setSN(requestTxn.getSN());
		}
		if(responseTxn.getStoreId() == null){
			responseTxn.setStoreId(requestTxn.getStoreId());
		}
		if(responseTxn.getTxnDate() == null){
			responseTxn.setTxnDate(requestTxn.getTxnDate());
		}
		if(responseTxn.getTxnNo() == null){
			responseTxn.setTxnNo(requestTxn.getTxnNo());
		}
		if(responseTxn.getTxnType() == null){
			responseTxn.setTxnType(requestTxn.getTxnType());
		}
		if(responseTxn.getUserId() == null){
			responseTxn.setUserId(requestTxn.getUserId());
		}
		if(responseTxn.getVoidTxnDate() == null){
			responseTxn.setVoidTxnDate(requestTxn.getVoidTxnDate());
		}
		if(responseTxn.getOriginalTxnNo() == null){
			responseTxn.setOriginalTxnNo(requestTxn.getOriginalTxnNo());
		}
		if(responseTxn.getBrandCode() == null){
			responseTxn.setBrandCode(requestTxn.getBrandCode());
		}
		
		if(responseTxn.getCouponDeductAmount() == null){
			responseTxn.setCouponDeductAmount(requestTxn.getAmount());
		}
		
//		responseTxn.setAllowedTenders("1.2.3");
//		responseTxn.setCustomerReceipt("Customer Receipt");
//		responseTxn.setStoreReceipt("Store Receipt");
//		responseTxn.setEjReceipt("Ej Receipt");
		return response;
	}
	

	/**
	 * If path is a directory, JMSTask would search all files in this folder. If recursive is true, 
	 * it also would search sub-folder by recursive type.
	 * once 
	 * @return
	 */
	public File getPath() {
		return path;
	}

	/**
	 * <p>
	 * If path is a directory, JMSTask would search all files in this folder. If recursive is true, 
	 * it also would search sub-folder by recursive type.
	 * 
	 * <p>
	 * If {@link this#operate(Object)}'s parameter is a type of File, the method would not be active, operate
	 *  method would use operate's parameter for searching.
	 * @param path
	 */
	public void setPath(File path) {
		this.path = path;
	}

	/**
	 * <p>
	 * If {@link this#path} is a directory, and the directory has some children directory, 
	 * it would be active if recursive is true. It will search file by recursive.
	 * @return
	 */
	public boolean isRecursive() {
		return recursive;
	}

	/**
	 * If {@link this#path} is a directory, and the directory has some children directory, 
	 * it would be active if recursive is true. It will search file by recursive.
	 * @param recursive
	 */
	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
		if(this.recursive){
			logger.debug("Enable recursive searched type");
		}else{
			logger.debug("Disable recursive searched type");
		}
	}

	/**
	 * Using Regular Expression to filter file name.
	 * @return
	 */
	public String getFileNameExpression() {
		return fileNameExpression;
	}

	/**
	 * Set regular expression to filter file name.
	 * @param fileNameExpression
	 */
	public void setFileNameExpression(String fileNameExpression) {
		this.fileNameExpression = fileNameExpression;
	}
	

	private final class ExpressionFilenameFilter implements FilenameFilter{
		private final Pattern pattern;
		
		public ExpressionFilenameFilter(String expression){
			this.pattern = Pattern.compile(expression);
		}
		
		@Override
		public boolean accept(File dir, String name) {
			boolean result = pattern.matcher(name).find();
			if(!result){
				logger.warn("Exclusive file(" + name + ") in directory(" + dir.getPath() + ")");
			}
			
			return result;
		}
		
	}
	
	private void uploadToJMSQueue(Interface data, Session session, MessageProducer producer) throws JMSException, IOException, ClassNotFoundException{
		Message message = session.createObjectMessage(data) ;
		producer.send(message);
		session.commit();
	}
	
	public QueueConnectionFactory getQueueConnectionFactory() {
		return queueConnectionFactory;
	}

	public void setQueueConnectionFactory(
			QueueConnectionFactory queueConnectionFactory) {
		this.queueConnectionFactory = queueConnectionFactory;
	}
	
	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	

	@Override
	public void run() {
		operate(null);
	}
	
	public static void main(String[] args) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date = format.format(new Date()) ;
		System.out.println(date);
	}
}
