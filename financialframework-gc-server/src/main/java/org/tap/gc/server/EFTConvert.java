package org.tap.gc.server;

import java.util.HashMap;
import java.util.Map;

import org.tap.gc.Converter;
import org.tap.gc.beans.Transaction;
import org.tap.gc.json.JSONHelper;
import org.tap.server.datasource.model.Cashier;
import org.tap.server.datasource.model.EFTPaymentData;
import org.tap.server.datasource.model.POS;
import org.tap.server.datasource.model.Server;
import org.tap.server.datasource.model.Store;


/**
 * 
 * @author Jacky.LIU
 *
 */
public class EFTConvert {
	public static final String TENDER_TYPE = "GC";
	
	public static final String TXNTYPE_SELLCOUPON = "Coupon Active";
	public static final String TXNTYPE_REDEEMCOUPON = "Coupon Redemption";
	
	public static EFTPaymentData toEFTPaymentData(boolean isConfirmed, Transaction data){
		EFTPaymentData eftData = new EFTPaymentData();
		eftData.setApprovalCode(data.getApprovalCode());
		eftData.setCardNumber(data.getCardNumber());
		eftData.setCardType(data.getCardType());
		if(null != data.getCashierId())
			eftData.setCashierInfo(Cashier.build(data.getCashierId()));
		eftData.setConfirm(isConfirmed);
		eftData.setExpiryDate(data.getExpiryDate());
		eftData.setOffline(data.isOffline());
		eftData.setPosInfo(POS.build(data.getPosId()));
		eftData.setServerInfo(Server.build(data.getServerId()));
		eftData.setStoreInfo(Store.build(data.getStoreId()));
		eftData.setTenderType(TENDER_TYPE);
		eftData.setBrandCode(data.getBrandCode());
		// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate +++
		if(data.getAuthDate() != null)
			eftData.setAuthDate(data.getAuthDate());
		if(data.getConfirmDate() != null)
			eftData.setConfirmDate(data.getConfirmDate());
		// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate ---
		
		org.tap.server.datasource.model.Transaction txn = new org.tap.server.datasource.model.Transaction();
		if(null != data.getAmount())
			txn.setAmount(Converter.getDoubleAmountFromLongValue(data.getAmount()));
		if(null != data.getCouponAmount())
			txn.setCouponAmount(Converter.getDoubleAmountFromLongValue(data.getCouponAmount()));
		if(null != data.getForfeitAmount())
			txn.setForfeitAmount(Converter.getDoubleAmountFromLongValue(data.getForfeitAmount()));
		if(null != data.getCouponDeductAmount())
			txn.setActualAmount(Converter.getDoubleAmountFromLongValue(data.getCouponDeductAmount()));
		
		if(null == data.getCouponAmount() && null != data.getAmount()){
			txn.setCouponAmount(Converter.getDoubleAmountFromLongValue(data.getAmount()));
		}
		txn.setBusDate(data.getBusDate());
		txn.setCouponID(data.getCouponUID());
		txn.setSn(data.getSN());
		txn.setTender(TENDER_TYPE);
		txn.setTxnDate(data.getTxnDate());
		txn.setTxnId(data.getTxnNo());
		if(data.getTxnType().equals(JSONHelper.TXNTYPE_SELLCOUPON)){
			txn.setTxnType(TXNTYPE_SELLCOUPON);
		}else if(data.getTxnType().equals(JSONHelper.TXNTYPE_REDEEMCOUPON)){
			txn.setTxnType(TXNTYPE_REDEEMCOUPON);
		}else{
			txn.setTxnType(data.getTxnType());
		}
		if(data.getCouponCount() != null)
			txn.setCouponCount(data.getCouponCount());
		
		
		eftData.setTransactionInfo(txn);
		return eftData;
	}
	
	public static Map<String, Object> toConfirmedReferKeys(Transaction data){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("SN", data.getSN());
		return map;
	}
}
