package org.tap.gc.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tap.common.execsql.SystemDate;
import org.tap.communication.IPacket;
import org.tap.communication.Receipt;
import org.tap.gc.ErrorCode;
import org.tap.gc.OpCodeConstants;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.BillerInfo;
import org.tap.gc.beans.BillerInfoList;
import org.tap.gc.beans.BillsPayment;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Member;
import org.tap.gc.beans.Page;
import org.tap.gc.beans.Transaction;
import org.tap.gc.beans.Transactions;
import org.tap.gc.json.JSONHelper;
import org.tap.gc.json.ReadProperties;
import org.tap.gc.security.ISecurity;
import org.tap.gc.security.support.NonSecurity;
import org.tap.svawebservicegateway.ISVAWebServiceGateway;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tap.operation.IOperation;
import com.tap.operation.OperationBase;
import com.tap.operation.OperationException;

import org.tap.tapwebservice.TAPWebServiceClient;
/**
 * It will handle IPacket from TCPListener, and return IReceipt to TCPListener.
 * It will be load into FS service
 * 
 * @author Jacky.LIU
 * 
 */
public class GCOperation extends OperationBase {
	private BufferData bf;
	private final FSCommunicationOperation communication;
	private  TAPWebServiceClient tapWsGateway;

	private QueueConnectionFactory queueConnectionFactory;

	private String queueName;

	private final ISVAWebServiceGateway svaGateway;

	private IOperation serialization;

	private IOperation confirmSerialization;

	private static Logger logger = Logger.getLogger(GCOperation.class);

	public IOperation getConfirmSerialization() {
		return confirmSerialization;
	}

	public void setConfirmSerialization(IOperation confirmSerialization) {
		this.confirmSerialization = confirmSerialization;
	}

	private boolean memberLoginPassword = true;

	private String encoding = "UTF-8";

	private ISecurity security;

	private Connection connection;

	private Queue queue;

	private MessageProducer messageProducer;

	private Session session;

	private int retryCount = 3;

	private long retryInterval = 500;

	private Object locker = new Object();

	private int retryCountToSVAService = 3;

	private String[] needToRetryTxnTypes = new String[]{JSONHelper.TXNTYPE_REDEEMCOUPON, JSONHelper.TXNTYPE_SELLCOUPON, JSONHelper.TXNTYPE_CONFIRM};

	private String[] needToRuturnSuccessTxnTypes = new String[]{JSONHelper.TXNTYPE_CONFIRM};

	private String[] needToRuturnSuccessBusTypes = new String[]{OpCodeConstants.BUSINESSTYPE_TELECOSERVER};

	private static final String BEEP_PREFIX = "908";

	private static final String DefaultFilePath = System.getProperty("user.dir") + File.separatorChar + "conf" + File.separatorChar + "messagemapping.properties" ;

	private static ReadProperties rp = ReadProperties.getInstall(DefaultFilePath);

	public GCOperation(ISVAWebServiceGateway svaGateway,FSCommunicationOperation communication){
		this.svaGateway = svaGateway;
		this.communication = communication ;
		bf = new BufferData();
	}

	public GCOperation(TAPWebServiceClient tapWsGateway,FSCommunicationOperation communication,ISVAWebServiceGateway svaGateway){
		this.svaGateway = svaGateway;
		this.tapWsGateway = tapWsGateway;
		this.communication = communication ;
		bf = new BufferData();
	}

	//	public GCOperation() throws DAOException{
	//		this(GatewayContext.lookup() , new FSCommunicationOperation());
	//	}

	public ISecurity getSecurity() {
		if(null == security)
			security = new NonSecurity();
		return security;
	}

	public String[] getNeedToRetryTxnTypes() {
		return needToRetryTxnTypes;
	}

	public void setNeedToRetryTxnTypes(String[] needToRetryTxnTypes) {
		this.needToRetryTxnTypes = needToRetryTxnTypes;
	}

	public void setSecurity(ISecurity security) {
		this.security = security;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public long getRetryInterval() {
		return retryInterval;
	}

	public void setRetryInterval(long retryInterval) {
		this.retryInterval = retryInterval;
	}

	public int getRetryCountToSVAService() {
		return retryCountToSVAService;
	}

	public void setRetryCountToSVAService(int retryCountToSVAService) {
		this.retryCountToSVAService = retryCountToSVAService;
	}

	public String[] getNeedToRuturnSuccessTxnTypes() {
		return needToRuturnSuccessTxnTypes;
	}

	public void setNeedToRuturnSuccessTxnTypes(String[] needToRuturnSuccessTxnTypes) {
		this.needToRuturnSuccessTxnTypes = needToRuturnSuccessTxnTypes;
	}

	public String[] getNeedToRuturnSuccessBusTypes() {
		return needToRuturnSuccessBusTypes;
	}

	public void setNeedToRuturnSuccessBusTypes(String[] needToRuturnSuccessBusTypes) {
		this.needToRuturnSuccessBusTypes = needToRuturnSuccessBusTypes;
	}

	@Override
	public Object operate(Object info) {
		if (!(info instanceof IPacket))
			throw new IllegalArgumentException(
					"info is not an instance of IPacket.");
		IPacket packet = (IPacket) info;
		Interface request = null;
		Interface requestInq = null; 
		Interface response = null;
		try {
			String requestXml = getSecurity().decode((String) packet.getReceived(), encoding);
			logger.debug("FF request data: " + requestXml);
			request= (Interface) XmlHelper
					.unmarshaller(requestXml);
			logger.debug("Operating OpCode: " + request.getOpCode());

			fillStoreCode(request);
			fillBrandCode(request);
			fillPage(request);

			Transaction tr = request.getTransaction();
			if(tr != null){
				boolean offline = tr.isOffline();
				if(offline == true){
					try {

						//FIXME !!!!! 1923 FS - EFTPAYMENTDATA
						processEFTPaymentData(request);
					} catch (JMSException e) {
						logger.error(e.getMessage());
						if(null == serialization){
							logger.warn("Lost: " + XmlHelper.marshaller(request));
						}else{
							serialization.operate(request);
						}
					}
					Receipt receipt = new Receipt(packet,
							getSecurity().encode(requestXml, encoding));
					receipt.setBusiness("GC");
					receipt.setAction("");
					return receipt;
				}
			}

			int opcode = request.getOpCode();
			String bussType = request.getBusinessType();
			//TODO 1360 BEEP FS Top-up and Payment

			if (((bussType.equals(OpCodeConstants.BUSINESSTYPE_BEEP_TOPUP)) || (bussType
					.equals(OpCodeConstants.BUSINESSTYPE_BEEP_PAYMENT)))) {


				Integer beepOpcode = request.getOpCode();

				//BEEP - First entry to FS - just to get BusDate from Storeline file
				if (request.getBeepRequest().getBusDate() == null) {

					String busDate = getBusinessDateFromStorelineFile();
					request.getBeepRequest().setBusDate(busDate);

					request.setResponseCode("0");
					request.setErrorMessage("Success");

					String res = XmlHelper.marshaller(request);


					processEFTPaymentData(request);

					Receipt receipt = new Receipt(packet, getSecurity().encode(
							res, encoding));
					receipt.setBusiness("GC");
					receipt.setAction(request.getOpCodeDescription());
					return receipt;

					//BEEP - Second entry to FS - to save in DB the Response from the BEEP device
				} else if ((request.getBeepResponse() != null)
						&&
						(beepOpcode.equals(OpCodeConstants.SellCouponCode)
								||
								beepOpcode.equals(OpCodeConstants.RedeemCouponCode))) {

					// TODO error code mapping

					String beepResultCode = request.getBeepResponse().getResultCode();	

					String beepResponseCode = null;

					if(beepResultCode.equals("00")){
						beepResponseCode="0";
						request.setErrorMessage("Success");
					}else{
						beepResponseCode = BEEP_PREFIX+beepResultCode;						
						String errorMessageBeep = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + beepResponseCode , "Error in Processing Beep Request");
						request.setErrorMessage(errorMessageBeep);
					}

					request.setResponseCode(beepResponseCode);


					String res = XmlHelper.marshaller(request);
					processEFTPaymentData(request);
					Receipt receipt = new Receipt(packet, getSecurity().encode(
							res, encoding));
					receipt.setBusiness("GC");
					receipt.setAction(request.getOpCodeDescription());
					return receipt;

					//BEEP - Third Entry to FS - to save Confirmation in DB
				}else if (beepOpcode.equals(OpCodeConstants.ConfirmCode)) {			

					logger.debug("Inside Confirmation FS");
					request.setResponseCode("0");
					request.setErrorMessage("Success");

					String res = XmlHelper.marshaller(request);
					processEFTPaymentData(request);
					Receipt receipt = new Receipt(packet, getSecurity().encode(
							res, encoding));
					receipt.setBusiness("GC");
					receipt.setAction(request.getOpCodeDescription());
					return receipt;
				}

			}

			
			// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate +++
			if(opcode == OpCodeConstants.ConfirmCode){
				tr.setConfirmDate(SystemDate.getSystemDateStr("yyyy-MM-dd HH:mm:ss"));
			}else if(opcode == OpCodeConstants.SellCouponCode || opcode == OpCodeConstants.RedeemCouponCode){
				tr.setAuthDate(SystemDate.getSystemDateStr("yyyy-MM-dd HH:mm:ss"));
				logger.debug("opcode is : "+ opcode);
			}
			// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate ---

			String responsexml = "" ;
			String requestRefNo = "" ;	
			String planCode = "" ;
			String transactionRRN = "" ;

			logger.debug("INQ Workaround bussType: " + bussType);
			logger.debug("INQ Workaround BUSINESSTYPE_TELCO_INQUIRY " + bussType.equals(OpCodeConstants.BUSINESSTYPE_TELCO_INQUIRY));
			logger.debug("INQ Workaround KEY_TRANSACTIONINQUIRY_OPCODE: " + OpCodeConstants.KEY_TRANSACTIONINQUIRY_OPCODE);


			/*
			 * Workaround so that it does not need to go back to FF for validation
			 * Changes Opcode 202 to 190 for TelcoInquiry
			 */
			try{
				if(bussType.equals(OpCodeConstants.BUSINESSTYPE_TELCO_INQUIRY)){
					logger.debug("INQ Workaround 1st Setting to: " + OpCodeConstants.KEY_TRANSACTIONINQUIRY_OPCODE);
					requestInq=request;
					request.setOpCode(OpCodeConstants.KEY_TRANSACTIONINQUIRY_OPCODE);
				}
				logger.debug("INQ Workaround 1st OpCode: " + request.getOpCode());
				logger.debug("INQ Workaround 1st request: " + request.toString());

			}
			catch(Exception e){	
				logger.error("Error in TelcoInquiry Workaround: " + request.toString());
			}
			/*
			 * End of workaround	
			 */
						
			if((bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION) || 
					bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL)) 
					&& opcode == OpCodeConstants.SellCouponCode){
				logger.debug("Process RRC Selling, sending request to tapws..");
				response = processTapWsRequest(request);
			}else if (bussType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER) &&
					tr.getSubBusinessType().equals(OpCodeConstants.SUBBUSINESSTYPE_BILLSPAYMENTINQUIRY)){
				
				//TODO 1293 - VALIDATE RETRY FLAG

// Mash Look 				
				
				InquiryFunction.validateRetryFlag(request);
				response = processSVA(request);
			}else{
				response = processSVA(request);
			}
			
			logger.debug("Response: "+XmlHelper.marshaller(response));
			String responseCode = response.getResponseCode() ;
			Integer workaroundOpCode = response.getOpCode();

			String errorMessage1 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Undefined error description");
			if(!responseCode.equals("90601") && !responseCode.equals("90602")){
				response.setErrorMessage(errorMessage1);
			}

			/*
			 * Resends a Transaction Inquiry with Opcode 202 from workaround opcode 190
			 */
			if(workaroundOpCode==OpCodeConstants.KEY_TRANSACTIONINQUIRY_OPCODE){
				String respCodeWorkaround = response.getResponseCode();				
				logger.debug("INQ Workaround respCodeWorkaround:"+respCodeWorkaround);

				if(respCodeWorkaround.equals("0")){
					requestInq.setOpCode(OpCodeConstants.CouponInfoCode);
					logger.debug("INQ Workaround 2nd requestInq: "+requestInq);
					response = processSVA(requestInq);
					logger.debug("INQ Workaround 2nd Response: "+XmlHelper.marshaller(response));
				}

				logger.debug("INQ Workaround 2nd OpCode: " + requestInq.toString());
				responseCode = response.getResponseCode() ;
				errorMessage1 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Undefined error description");
				response.setErrorMessage(errorMessage1);
			}
			/*
			 * End of workaround
			 */


			Transaction transaction1 =  response.getTransaction();
			if(transaction1 != null)
			{
				logger.debug("if transaction1 !=null");
				//				transaction.setReferenceNo(requestRefNo);
				//				transaction.setProdCode(planCode);
				//				transaction.setTransactionRRN(transactionRRN);
				requestRefNo = transaction1.getReferenceNo();
				planCode = transaction1.getProdCode();
				transactionRRN = transaction1.getTransactionRRN();
			}


			if(bussType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER)){
				response.setOpCode(request.getOpCode());
				if(("0".equals(responseCode) || "00".equals(responseCode)) ){
					String errorMessage = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Undefined error description");
					response.setErrorMessage(errorMessage);					
				}				

				try {
					processEFTPaymentData(response);
					logger.debug("Processed Bills Payment Data Saving: "+XmlHelper.marshaller(response));	
				} catch (JMSException e) {
					logger.error(e.getMessage());
					if(null == serialization){
						logger.warn("Lost: " + XmlHelper.marshaller(response));
					}else{
						serialization.operate(response);
						logger.debug("responsecode0 done serialization [billspayment]: "+XmlHelper.marshaller(response));
					}
				}

			}else if(("0".equals(responseCode) || "00".equals(responseCode)) || bussType.equals(OpCodeConstants.BUSINESSTYPE_TELCO_INQUIRY)){
				logger.debug("Operated OpCode: " + response.getOpCode() + " successfully");

				response.setOpCode(request.getOpCode());
				String errorMessage = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Undefined error description");
				response.setErrorMessage(errorMessage);

				if(bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION) || 
						bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL)) {
				}
				try {
					processEFTPaymentData(response);
					logger.debug("responsecode0 done processEFTPaymentData: "+XmlHelper.marshaller(response));					
				} catch (JMSException e) {
					logger.error(e.getMessage());
					if(null == serialization){
						logger.warn("Lost: " + XmlHelper.marshaller(response));
					}else{
						serialization.operate(response);
						logger.debug("responsecode0 done serialization: "+XmlHelper.marshaller(response));
					}
				}
			}else{
				logger.warn("Operated OpCode: " + response.getOpCode() + " failure");
				Transaction transaction =  request.getTransaction();
				if(transaction != null){
					transaction.setReferenceNo(requestRefNo);
					transaction.setProdCode(planCode);
					transaction.setTransactionRRN(transactionRRN);
				}

				request.setOpCode(request.getOpCode());
				request.setResponseCode(responseCode);
				String errorMessage = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Undefined error description");
				request.setErrorMessage(errorMessage);

				if(opcode == OpCodeConstants.ConfirmCode){
					if( !bussType.equals(OpCodeConstants.BUSINESSTYPE_TELECOSERVER) && !bussType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER) 
							&& (!bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION) && !bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL))){
						request.setResubmit("1");
					}
				}
				try {
					processEFTPaymentData(request);
				} catch (JMSException e) {
					logger.error(e.getMessage());
					if(null == serialization){
						logger.warn("Lost: " + XmlHelper.marshaller(request));
					}else{
						serialization.operate(request);
					}
				}

				if(opcode == OpCodeConstants.ConfirmCode){
					if( !bussType.equals(OpCodeConstants.BUSINESSTYPE_TELECOSERVER) && !bussType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER  ) 
							&& !bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION)  &&
							(!bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL)	)){
						logger.debug("exec gc egc error confirm.");
						if(null == confirmSerialization){
							try{
								logger.warn("Lost: " + XmlHelper.marshaller(request));
							}catch(Exception e1){
								logger.error(e1);
							}
						}else{
							logger.debug("confirm request   serialization .");
							confirmSerialization.operate(request);
						}
						if(!bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION)  &&
								!bussType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL)	)
						{
							request.setErrorMessage("Success");
							request.setResponseCode("0");
							response = request ;
						}	
					}
				}
			}

			response = "{
				      "ResponseCode" : "0",
				      "RecordCount" : 1031,
				      "PageCount" : 0,
				      "Stores" : [
				         {
				            "StoreID" : "1",
				            "StoreAttribute3" : "SA03",
				            "StoreAttribute8" : "",
				            "StoreAttribute4" : "SA04",
				            "StoreAttribute2" : "SA02",
				            "StoreCode" : "0001",
				            "StoreAttribute7" : "",
				            "StoreAttribute1" : "SA01",
				            "StoreProvince" : "Happy Valley",
				            "StoreLongitude" : "114.1841804",
				            "StoreLatitude" : "22.2695641",
				            "StoreAddressDetail" : "G/F., Winner House,15 Wong Nei Chung Road, Happy Valley, HK",
				            "StoreAttribute5" : "SA05",
				            "StoreAttribute9" : "",
				            "StoreCountry" : "HK",
				            "StoreAttribute6" : "SA06",
				            "StoreOpenTime" : ""
				         },
				         {
				            "StoreID" : "2",
				            "StoreAttribute3" : "SA03",
				            "StoreAttribute8" : "",
				            "StoreAttribute4" : "SA04",
				            "StoreAttribute2" : "SA02",
				            "StoreCode" : "0008",
				            "StoreAttribute7" : "",
				            "StoreAttribute1" : "",
				            "StoreProvince" : "Shau Kei Wan",
				            "StoreLongitude" : "114.223464",
				            "StoreLatitude" : "22.280592",
				            "StoreAddressDetail" : "Shop C, G/F., Elle Bldg., 192-198 Shaukiwan Road, Shaukiwan, HK",
				            "StoreAttribute5" : "SA05",
				            "StoreAttribute9" : "",
				            "StoreCountry" : "HK",
				            "StoreAttribute6" : "",
				            "StoreOpenTime" : ""
				         },
				         (...)
				      ],
				      "Action" : "GetStoreList"
				   }"
			
			String res = XmlHelper.marshaller(response);
			logger.debug("FS response data:"+ res) ;
			Receipt receipt = new Receipt(packet,
					getSecurity().encode(res, encoding));
			receipt.setBusiness("GC");
			receipt.setAction(response.getOpCodeDescription());
			return receipt;
			//		} catch (JAXBException e) {
			//			logger.error(e.getMessage(), e);
			//			return this.handleConfirmException(packet, request, e) ;
			////			String errorCode = ErrorCode.CODE_SYSTEMERROR ;
			////			String errorMessage = ErrorCode.getErrorMessage( errorCode);
			////			request.setResponseCode(errorCode); 
			////			request.setErrorMessage(errorMessage);
			////			processPaymentData(request);
			////			
			////			throw new OperationException(e);
			//		} catch (JSONException e) {
			//			logger.error("Operated OpCode: " + request.getOpCode() + " failure, exception: " + e.getMessage());
			//			logger.error(e.getMessage(), e);
			//			return this.handleConfirmException(packet, request, e) ;
			////			String errorCode = ErrorCode.CODE_SYSTEMERROR ;
			////			String errorMessage = ErrorCode.getErrorMessage( errorCode);
			////			request.setResponseCode(errorCode); 
			////			request.setErrorMessage(errorMessage);
			////			processPaymentData(request);
			////			
			////			throw new OperationException(e);
			//		} catch (RemoteException e) {
			//			logger.error("Operated OpCode: " + request.getOpCode() + " failure, exception: " + e.getMessage());
			//			logger.error(e.getMessage(), e);
			//			return this.handleConfirmException(packet, request, e) ;
			//			String errorCode = ErrorCode.CODE_NETWORKCABLEERROR ;
			//			String errorMessage = ErrorCode.getErrorMessage( errorCode);
			//			request.setResponseCode(errorCode); 
			//			request.setErrorMessage(errorMessage);
			//			processPaymentData(request);
			//			
			//			throw new OperationException(e);
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("Operated OpCode: " + request.getOpCode() + " failure, exception: " + e.getMessage());
			logger.error(e.getMessage(), e);
			return this.handleConfirmException(packet, request, e) ;
			//			
			//			String errorCode = ErrorCode.CODE_SYSTEMERROR ;
			//			String errorMessage = ErrorCode.getErrorMessage( errorCode);
			//			request.setResponseCode(errorCode); 
			//			request.setErrorMessage(errorMessage);
			//			processPaymentData(request);
			//			
			//			throw new OperationException(e);
		}
	}

	//FIXME 1244 90414 error not being processed

	private Receipt handleConfirmException(IPacket packet, Interface request , Exception e){
		logger.debug("exec handleConfirmException...") ;
		int opcode = request.getOpCode();
		//		String responseCode = request.getResponseCode();
		String bussType = request.getBusinessType();
		Receipt receipt = null ;
		try{
			logger.debug("starting handleConfirmException...Opcode: "+opcode +
					" BusType: "+bussType) ;
			if(opcode == OpCodeConstants.ConfirmCode){
				logger.error("Exception to be processed at opcode 100: "+e);
				//1244 as per Tass, only GC/EGC should be resubmitted
				if( !bussType.equals(OpCodeConstants.BUSINESSTYPE_TELECOSERVER) && !bussType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER  ) ){
					//				if( bussType.equals(OpCodeConstants.BUSINESSTYPE_GCSERVER) || bussType.equals(OpCodeConstants.BUSINESSTYPE_EGCSERVER) ){ 
					//					logger.debug("gc/egc server  confirm handle error .") ;
					logger.debug("Start of Non-Telco or Non-BillsPayment server exception handling.") ;
					Interface newInterface = request ;
					String errorInfo = "" ;
					if(e != null){
						errorInfo = e.toString();
					}
					
					if(errorInfo == null){
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
					}else if(errorInfo.contains("javax.xml.bind.UnmarshalException")){
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_UNMARSHALLERERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_UNMARSHALLERERROR);
					}else if(errorInfo.contains("javax.xml.bind.MarshalException")){
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_MARSHALLERERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_MARSHALLERERROR);
					}else if(errorInfo.contains("Read timed out") || e.getMessage().contains("Executed timeout")){
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_READTIMEOUTERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_READTIMEOUTERROR);
					}else if (errorInfo.contains("org.apache.axis2.AxisFault")
						|| e.getMessage().contains("java.net.ConnectException") 
						|| e.getMessage().contains("java.net.SocketException")) {
						
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
					}else if(errorInfo.contains("java.net.NoRouteToHostException")){
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_NETWORKCABLEERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_NETWORKCABLEERROR);
					}else{
						String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
						newInterface.setErrorMessage(errorMessage);
						newInterface.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
					}

					newInterface.setResubmit("1");
					//save DB .
					processPaymentData(newInterface) ;

					if(null == confirmSerialization){
						try{
							logger.warn("Lost: " + XmlHelper.marshaller(request));
						}catch(Exception e1){
							logger.error(e1);
						}
					}else{
						//TODO 1293 CHECK DB - RESUBMIT
						logger.debug("confirm request serialization .");
						confirmSerialization.operate(request);
					}
					request.setErrorMessage("Success");
					request.setResponseCode("0");
				}else{
					logger.debug("No action for TelcoServer or BillsPayment(old");
				}

			}else{
				logger.debug("Exception processing for non-Opcode 100. Details: "+e) ;
				String errorInfo = "" ;
				if(e != null){
					errorInfo = e.toString();
				}
							
				if(errorInfo == null){
					logger.debug("is null") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
				}else if(errorInfo.contains("javax.xml.bind.UnmarshalException")){
					logger.debug("is javax.xml.bind.UnmarshalException") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_UNMARSHALLERERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_UNMARSHALLERERROR);
				}else if(errorInfo.contains("javax.xml.bind.MarshalException")){
					logger.debug("is javax.xml.bind.MarshalException") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_MARSHALLERERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_MARSHALLERERROR);
				}else if(errorInfo.contains("Read timed out") || e.getMessage().contains("Executed timeout")){
					logger.debug("is Read timed out") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_READTIMEOUTERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_READTIMEOUTERROR);
				}else if (errorInfo.contains("org.apache.axis2.AxisFault")
					|| e.getMessage().contains("java.net.ConnectException") 
					|| e.getMessage().contains("java.net.SocketException")) {
									
					logger.debug("is org.apache.axis2.AxisFault") ;

					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_CONNECTTOWEBSERVICEERROR);
				}else if(errorInfo.contains("java.net.NoRouteToHostException")){
					logger.debug("is java.net.NoRouteToHostException") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_NETWORKCABLEERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_NETWORKCABLEERROR);
				}else if(errorInfo.contains("Retry Invalid")){
					logger.debug("Last transaction has already been retried");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_BILLSPAYMENT_INVALIDRETRY);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_BILLSPAYMENT_INVALIDRETRY);
				}else if(errorInfo.contains("No records found")){
					logger.debug("No records found to inquire");
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_BILLSPAYMENT_NORECORDSFOUND);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_BILLSPAYMENT_NORECORDSFOUND);
				}else{
				
					logger.debug("is system error") ;
					String errorMessage = ErrorCode.getErrorMessage(ErrorCode.CODE_SYSTEMERROR);
					request.setErrorMessage(errorMessage);
					request.setResponseCode(ErrorCode.CODE_SYSTEMERROR);
				}
				logger.debug("error save db") ;
				processPaymentData(request) ;
			}

			String res = XmlHelper.marshaller(request);
			logger.debug("FS response data:"+ res) ;
			receipt = new Receipt(packet,
					getSecurity().encode(res, encoding));
			receipt.setBusiness("GC");
			receipt.setAction("");

		}catch(Exception e2){
			logger.error("Exception at handleConfirmException: "+e2);
		}
		return receipt;
	}

	private void processPaymentData(Interface response){
		try {
			processEFTPaymentData(response);
		} catch (JMSException e) {
			logger.error(e.getMessage());
			if(null == serialization){
				try {
					logger.warn("Lost: " + XmlHelper.marshaller(response));
				} catch (JAXBException e1) {
					e1.printStackTrace();
				}
			}else{
				serialization.operate(response);
			}
		}
	}
	private Interface processTapWsRequest(Interface info) throws JSONException, JAXBException{
		//JSONObject request = JSONHelper.toJSONObject(info);

		logger.debug("Request to TAPWS: "+XmlHelper.marshaller(info));

		Interface response = requestToTAPWs(info);
		String xml = response!=null ? XmlHelper.marshaller(response) : "";

		return fillTransaction(info, response);

	}
	private Interface requestToTAPWs(Interface request){

// Mash SOAP Sample 		
		
		String xmlRequest = null;

		int opcode=request.getOpCode();
		if(opcode==OpCodeConstants.SellCouponCode){
			request.setSubAction("ValidateRRC");
			tapWsGateway.setAction("validateRRC");
		}else if(opcode==OpCodeConstants.ConfirmCode){
			request.setSubAction("UpdateRRC");
			tapWsGateway.setAction("updateRRC");
		}

		try {
			xmlRequest = XmlHelper.marshaller(request);
			logger.debug("Sent request: " + xmlRequest);

		} catch (JAXBException e) {
			logger.debug("Error in marshalling request :"+e.getMessage());

		}
		Interface response = tapWsGateway.sendRequest(xmlRequest);
		logger.debug("Received response: " + response);
		return response;		

	}
	
	private Interface processSVA(Interface info) throws JSONException, RemoteException, JAXBException {
		if(info.getOpCode() == OpCodeConstants.MemberLoginCode){
			return processMemberLogin(info);
		}

		//		if(info.getOpCode() == OpCodeConstants.ConfirmCode){
		//			String businessType = info.getBusinessType();
		//			if(businessType.equals(OpCodeConstants.BUSINESSTYPE_TELECOSERVER)){
		//				String requestxml = XmlHelper.marshaller(info);
		//				String rs = (String) this.communication.operate(requestxml);
		//				Interface response = (Interface) XmlHelper.unmarshaller(rs);
		//				String responseCode = response.getResponseCode() ;
		//				if("0".equals(responseCode) || "00".equals(responseCode)){
		//					logger.debug("CFS Operated OpCode: " + response.getOpCode() + " successfully");
		//				}else{
		//					logger.warn("CFS Operated OpCode: " + response.getOpCode() + " failure");
		//					return fillTransaction(info, response);
		//				}
		//			}
		//		}
		//		
		JSONObject request = JSONHelper.toJSONObject(info);
		logger.debug("Request to SVA: "+request);
		
		JSONObject response = requestToSVA(request);

		//sample telcoinquiry response
		/*JSONObject response = null;
		String str = "{\"retailerDeduct\":\"0\",\"amount\":\"10\",\"respCode\":\"fds78\",\"targetSubsAccount\":\"09288519159\",\"retailerNewBalance\":\"0\",\"requestRefNo\":\"1234567890\",\"branchID\":\"RDS000000000007033\",\"planCode\":\"LOAD\",\"transactionRRN\":\"520350915223\",\"transactionTimestamp\":\"1430696428248\",\"corporateID\":\"RDS INC 2\",\"respCodeDesc\":\"04-May 07:40: Sorry, unable to process. Please contact SMART Hotline. This message is free. Ref:520350915223\"}";
		List<String> txnList = new ArrayList<String>();
		txnList.add(str);
		try {
			JSONObject o2 = new JSONObject();
			request.put("ResponseCode", "90504");
			request.put("TelcoType","Smart");
			request.put("transactionRRN", str);
			request.put("respCodeDesc", "respCodeDesc");
			request.put("ProdCode", "INQ");

			response=request;
			System.out.println("response from o2; "+response.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}*/

		//End sample telcoinquiry response
		
		logger.debug("processsva response: "+response);
		String busType=info.getBusinessType();
		int opCode = info.getOpCode();

		//for testing
		/*		
				opCode=202;
				response.put(OpCodeConstants.KEY_RESPONSECODE, 90504);
				System.out.println((response!=null)+(OpCodeConstants.BUSINESSTYPE_TelcoInquiry)+(opCode==OpCodeConstants.CouponInfoCode));
		 */		
		//end testing

		if(response!=null && busType.equals(OpCodeConstants.BUSINESSTYPE_TelcoInquiry) && opCode==OpCodeConstants.CouponInfoCode){
			logger.debug("telco process response: "+response);
			String bussinessType= response.getString(OpCodeConstants.KEY_BUSINESSTYPE);
			if(bussinessType!=null && bussinessType.equals(OpCodeConstants.BUSINESSTYPE_TelcoInquiry)){
				String targetTransRes =null;
				int responseCode = response.getInt(OpCodeConstants.KEY_RESPONSECODE);
				logger.debug("responsecode : " + responseCode);

				//				String respCode=Integer.toString(responseCode);
				String respCode = null;	

				Interface inter = new Interface();
				Transactions txns = new Transactions();


				respCode=Integer.toString(responseCode);
				logger.debug("respCode to string: "+respCode);

				if(!respCode.equals("90504")){
					logger.debug("process successful response");
					try{
						if(response.has(OpCodeConstants.KEY_TRANSACTIONRRN)){
							targetTransRes = (String) response.get(OpCodeConstants.KEY_TRANSACTIONRRN);
							logger.debug("targettras: "+targetTransRes);
						}


						if(respCode.equals("0")){

							ArrayList<Transaction> list = new ArrayList<Transaction>();
							ArrayList<Transaction> list2 = (ArrayList<Transaction>) txns.getTransaction();
							//telcotype-workaround to identify if response if globe or smart;
							//if has telcotype response is smart, if no telcotype response is globe

							if(response.has(OpCodeConstants.KEY_TELCOTYPE)){
								parseSmartInquiryResult(list, targetTransRes);
							}else{
								list=parseGlobeTranstarget(list, targetTransRes );
							}
							sortList(list);
							int x=0;
							x=list.size()>5 ? x=5 : (x=list.size());
							for(int i=0; i<x; i++){
								list2.add(list.get(i));
							}
						}
					}catch(Exception e){
						logger.debug(e.getMessage());
						txns=null;
						respCode="90504";
					}


				}else if(respCode.equals("90504") && response.has(OpCodeConstants.KEY_TELCOTYPE)){
					logger.debug("process unsuccessful response");
					JSONArray targetTransResArr=null;
					try{
						if(response.has(OpCodeConstants.KEY_TRANSACTIONRRN)){
							targetTransResArr = response.getJSONArray(OpCodeConstants.KEY_TRANSACTIONRRN);
							logger.debug("targettras: "+targetTransRes);
						}
						if(response.has(OpCodeConstants.KEY_TELCOTYPE)){
							respCode=getErrorCode(targetTransResArr.toString());
							logger.debug("targettras: "+targetTransRes);
						}
					}catch(Exception e){
						logger.debug(e.getMessage());
						txns=null;
						respCode="90504";
					}
				}


				logger.debug("final responsecode: "+respCode);
				inter.setOpCode(OpCodeConstants.CouponInfoCode);
				inter.setBusinessType(OpCodeConstants.BUSINESSTYPE_TelcoInquiry);
				inter.setResponseCode(respCode);

				if((respCode.equals("90504"))||(respCode.equals("90505"))){
					//do nothing
				}else{
					inter.setTransactions(txns);
				}

				String telco =  XmlHelper.marshaller(inter);
				logger.debug("Telco Inquiry marshalled  response: "+telco);

				return inter;

			}
		}

		//FIXME !!! 1293 SVAWS TO FS
		if(response!=null && busType.equals(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER) 
				&& opCode==OpCodeConstants.SellCouponCode){
			try{

				String action = (String) response.get("Action");
				if(action.equals("ExecuteBillsPayment")){

					try{

						String responseCode = response.getString("ResponseCode");

						if(responseCode.equals("0")){

							logger.debug("Response from Exec CFS: "+response.toString());

							Interface inter = new Interface();
							Transaction trans = new Transaction();

							trans.setSKU(response.getString("Sku"));
							trans.setPosId(response.getString("PosId"));
							trans.setCashierId(response.getString("CashierId"));
							trans.setStoreId(response.getString("StoreId"));
							trans.setTxnDate(response.getString("TxnDate"));
							trans.setSN(response.getString("SN"));
							trans.setTxnNo(response.getString("TxnNo"));
							trans.setTotalAmount(response.getLong("TotalAmount"));
							trans.setItemUnitAmount(response.getLong("ItemUnitAmount"));
							trans.setTxnType(response.getString("TxnType"));
							trans.setBrandCode(response.getString("BrandCode"));
							trans.setSubBusinessType(response.getString("SubBusinessType"));

							JSONObject responseBP = (JSONObject) response.get("response");

							String errorLevel = responseBP.getString("error_level");

							BillsPayment bills = new BillsPayment();
							bills.setBizTokenId(responseBP.getString("biz_token_id"));
							bills.setCbciTransactionId(responseBP.getString("cbci_transaction_id"));
							bills.setErrorLevel(errorLevel);
							bills.setTransactionDatetime(responseBP.getString("transaction_datetime"));

							JSONArray resultDetailBP = (JSONArray) responseBP.get("result_detail");

							BillsPayment.ResultDetail resultDetail = new BillsPayment.ResultDetail();

							StringBuilder resultMessageSB = new StringBuilder();

							String delim = "";
							for (int i = 0; i < resultDetailBP.length(); i++) {								
								BillsPayment.ResultDetail.ResultDetails resultDetails = new BillsPayment.ResultDetail.ResultDetails();								
								String resultMessageRaw = resultDetailBP.getJSONObject(i).getString("result_message");								
								resultMessageSB.append(delim).append(resultMessageRaw);
								delim = ",";
								resultDetails.setResult(resultDetailBP.getJSONObject(i).getString("result"));
								resultDetails.setResultMessage(resultMessageRaw);								
								resultDetail.getResultDetails().add(resultDetails);
							}

							String resultMessage = resultMessageSB.toString().trim();

							bills.setResultDetail(resultDetail);

							BillsPayment.Receipt receiptBP = new BillsPayment.Receipt();

							Object receiptRaw = responseBP.get("receipt");

							if(!receiptRaw.equals(null)){

								JSONObject receipt = (JSONObject) receiptRaw;

								receiptBP.setReceiptitem1(receipt.getString("receiptitem1"));
								receiptBP.setReceiptitem2(receipt.getString("receiptitem2"));
								receiptBP.setReceiptitem3(receipt.getString("receiptitem3"));
								receiptBP.setReceiptitem4(receipt.getString("receiptitem4"));
								receiptBP.setReceiptitem5(receipt.getString("receiptitem5"));
								receiptBP.setReceiptitem6(receipt.getString("receiptitem6"));
								receiptBP.setReceiptitem7(receipt.getString("receiptitem7"));
								receiptBP.setReceiptitem8(receipt.getString("receiptitem8"));
								receiptBP.setReceiptitem9(receipt.getString("receiptitem9"));
								receiptBP.setReceiptitem10(receipt.getString("receiptitem10"));
								receiptBP.setReceiptitem11(receipt.getString("receiptitem11"));
								receiptBP.setReceiptitem12(receipt.getString("receiptitem12"));
								receiptBP.setReceiptitem13(receipt.getString("receiptitem13"));
								receiptBP.setReceiptitem14(receipt.getString("receiptitem14"));
								receiptBP.setReceiptitem15(receipt.getString("receiptitem15"));
								receiptBP.setReceiptitem16(receipt.getString("receiptitem16"));
								receiptBP.setReceiptitem17(receipt.getString("receiptitem17"));
								receiptBP.setReceiptitem18(receipt.getString("receiptitem18"));
								receiptBP.setReceiptitem19(receipt.getString("receiptitem19"));
								receiptBP.setReceiptitem20(receipt.getString("receiptitem20"));
								receiptBP.setReceiptitem21(receipt.getString("receiptitem21"));
								receiptBP.setReceiptitem22(receipt.getString("receiptitem22"));
								receiptBP.setReceiptitem23(receipt.getString("receiptitem23"));
								receiptBP.setReceiptitem24(receipt.getString("receiptitem24"));


								JSONArray variables = (JSONArray) receipt.get("variables");			
								BillsPayment.Receipt.Variables variablesBP = new BillsPayment.Receipt.Variables();

								for (int i = 0; i < variables.length(); i++) {
									BillsPayment.Receipt.Variables.Variableitems vis = new BillsPayment.Receipt.Variables.Variableitems();
									vis.setVariableitem(variables.getString(i).replace("{\"variableitem\":\"", "")
											.replace("\"}", "").replace("\\r", ""));								
									variablesBP.getVariableitems().add(vis);
								}

								receiptBP.setVariables(variablesBP);
								bills.setReceipt(receiptBP);
							}


							ObjectMapper mapper = new ObjectMapper();

							Map<String, Object> map = mapper.readValue(response.toString(), 
									new TypeReference<Map<String, Object>>() {});

							HashMap<String,String> billerInfoMap = (HashMap<String, String>) map.get("BillerInfo");

							BillerInfoList bInfoList = new BillerInfoList();
							List<BillerInfo> biListWithInput = bInfoList.getBillerInfo(); 

							for (Map.Entry<String, String> entry : billerInfoMap.entrySet())
							{		        		    
								BillerInfo bi = new BillerInfo();
								bi.setItemCode(entry.getKey());
								bi.setInputValue(entry.getValue());
								biListWithInput.add(bi);
							}

							inter.setBillsPayment(bills);
							inter.setBillerInfoList(bInfoList);
							inter.setTransaction(trans);
							inter.setOpCode(OpCodeConstants.SellCouponCode);
							inter.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);

							if(errorLevel.equals("N")){
								inter.setResponseCode("0");
								String errorMessage0 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "0" , "Success");
								inter.setErrorMessage(errorMessage0);						
							}else if(errorLevel.equals("W")){
								inter.setResponseCode("90601");
								String errorMessage90601 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90601" , "Validation Error: "+resultMessage);
								String errorMessageWhole = errorMessage90601+resultMessage;
								logger.debug("Setting error message to: "+errorMessageWhole);
								inter.setErrorMessage(errorMessageWhole);
							}else if(errorLevel.equals("E")){
								inter.setResponseCode("90602");
								String errorMessage90602 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90602" , "Bills Payment Error: "+resultMessage);
								String errorMessageWhole = errorMessage90602+resultMessage;
								logger.debug("Setting error message to: "+errorMessageWhole);
								inter.setErrorMessage(errorMessageWhole);
							}else{
								inter.setResponseCode("90600");
								String errorMessage90600 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90600" , "Error in Processing Bills Payment Request");
								inter.setErrorMessage(errorMessage90600);
							}

							return inter;
						}else{
							logger.error("Error in Processing CFS Bills Payment Response. ResponseCode: "+responseCode);
							Interface errorInterFace = new Interface();
							errorInterFace.setOpCode(OpCodeConstants.SellCouponCode);
							errorInterFace.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);
							errorInterFace.setResponseCode(responseCode);
							String errorMessageBills = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + responseCode , "Error in Processing Bills Payment Request");
							errorInterFace.setErrorMessage(errorMessageBills);
							return errorInterFace;

						}

					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error in Processing CFS Bills Payment Response. Details: "+e);
						Interface errorInterFace = new Interface();
						errorInterFace.setOpCode(OpCodeConstants.SellCouponCode);
						errorInterFace.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);
						errorInterFace.setResponseCode("90600");
						String errorMessage90600 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90600" , "Error in Processing Bills Payment Request");
						errorInterFace.setErrorMessage(errorMessage90600);
						return errorInterFace;
					}

				}else if (action.equals("GetRequiredBillerFields")){
					String responseCode = response.getString("ResponseCode");
					if(responseCode.equals("0")){
						logger.debug("Processing BillsPayment Opcode 101. Details: "+response.toString());				
						Interface inter = new Interface();
						BillerInfoList biList = new BillerInfoList();
						Transaction trans = new Transaction();
						JSONArray billerInfoList = (JSONArray) response.get("BillerInfoList");
						
						if (request.getString("SubBusinessType").equals(OpCodeConstants.SUBBUSINESSTYPE_BILLSPAYMENTPOSTREQ)){
							for (int i = 0; i < billerInfoList.length(); i++) {
								JSONObject bInfo = billerInfoList.getJSONObject(i);
								BillerInfo bi = new BillerInfo();
								bi.setItemName(bInfo.getString("itemName"));
								bi.setListItemValue(bInfo.getString("listItemValue"));
								bi.setItemType(bInfo.getString("itemType"));
								bi.setBillerCode(bInfo.getString("billerCode"));
								bi.setListItemCode(bInfo.getString("listItemCode"));
								bi.setBillerName(bInfo.getString("billerName"));
								bi.setDocumentVersion(bInfo.getString("documentVersion"));
								bi.setDigit(bInfo.getString("digit"));
								bi.setItemValue(bInfo.getString("itemValue"));
								bi.setListDisplayOrder(bInfo.getString("listDisplayOrder"));
								bi.setServiceCode(bInfo.getString("serviceCode"));
								bi.setDisplayOrder(bInfo.getInt("displayOrder"));
								bi.setRequired(bInfo.getString("required"));
								bi.setItemCode(bInfo.getString("itemCode"));
								bi.setBarcodeAllDigit(bInfo.getString("barcodeAllDigit"));
								bi.setBarcodeStartPos(bInfo.getString("barcodeStartPos"));
								bi.setBarcodeSize(bInfo.getString("barcodeSize"));
								bi.setDecimalFlag(bInfo.getString("decimalFlag"));
								biList.getBillerInfo().add(bi);
							}
						}else if (request.getString("SubBusinessType").equals(OpCodeConstants.SUBBUSINESSTYPE_BILLSPAYMENTINQUIRY)){
							for (int i = 0; i < billerInfoList.length(); i++) {
								JSONObject bInfo = billerInfoList.getJSONObject(i);
								BillerInfo bi = new BillerInfo();
								bi.setItemName(bInfo.getString("itemName"));
								bi.setListItemValue(bInfo.getString("listItemValue"));
								bi.setItemType(bInfo.getString("itemType"));
								bi.setBillerCode(bInfo.getString("billerCode"));
								bi.setListItemCode(bInfo.getString("listItemCode"));
								bi.setBillerName(bInfo.getString("billerName"));
								bi.setDocumentVersion(bInfo.getString("documentVersion"));
								bi.setDigit(bInfo.getString("digit"));
								bi.setItemValue(bInfo.getString("itemValue"));
								bi.setListDisplayOrder(bInfo.getString("listDisplayOrder"));
								bi.setServiceCode(bInfo.getString("serviceCode"));
								bi.setDisplayOrder(0);
								bi.setRequired(bInfo.getString("required"));
								bi.setItemCode(bInfo.getString("itemCode"));
								bi.setBarcodeAllDigit(bInfo.getString("barcodeAllDigit"));
								bi.setBarcodeStartPos(bInfo.getString("barcodeStartPos"));
								bi.setBarcodeSize(bInfo.getString("barcodeSize"));
								bi.setDecimalFlag(bInfo.getString("decimalFlag"));
								biList.getBillerInfo().add(bi);
							}
						}
	
			
						inter.setBillerInfoList(biList);
						inter.setOpCode(OpCodeConstants.CouponInfoCode);
						inter.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);

						trans.setSKU(response.getString("Sku"));
						trans.setPosId(response.getString("PosId"));
						trans.setCashierId(response.getString("CashierId"));
						trans.setStoreId(response.getString("StoreId"));
						trans.setTxnDate(response.getString("TxnDate"));
						trans.setSN(response.getString("SN"));
						trans.setTxnNo(response.getString("TxnNo"));
						trans.setTotalAmount(response.getLong("TotalAmount"));
						trans.setItemUnitAmount(response.getLong("ItemUnitAmount"));
						trans.setBrandCode(response.getString("BrandCode"));
						trans.setSubBusinessType(response.getString("SubBusinessType"));
						inter.setResponseCode(responseCode);
						inter.setTransaction(trans);
						
						return inter;						

					}else{						
						logger.error("Error in fetching Biller Info");
						Interface errorInterFace = new Interface();
						errorInterFace.setOpCode(OpCodeConstants.SellCouponCode);
						errorInterFace.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);
						errorInterFace.setResponseCode("90603");
						String errorMessage90603 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90603" , "Error in Fetching Biller Fields");
						errorInterFace.setErrorMessage(errorMessage90603);
						return errorInterFace;
					}

				}else{
					logger.error("Invalid Response from SVA Webservice");
					Interface errorInterFace = new Interface();
					errorInterFace.setOpCode(OpCodeConstants.SellCouponCode);
					errorInterFace.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);
					errorInterFace.setResponseCode("90600");
					String errorMessage90600 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90600" , "Error in Processing Bills Payment Request");
					errorInterFace.setErrorMessage(errorMessage90600);
					return errorInterFace;
				}


			}catch(Exception e){
				logger.error("Error in Processing CFS Bills Payment Response. Details: "+e);
				Interface errorInterFace = new Interface();
				errorInterFace.setOpCode(OpCodeConstants.SellCouponCode);
				errorInterFace.setBusinessType(OpCodeConstants.BUSINESSTYPE_BILLPAYMENTSERVER);
				errorInterFace.setResponseCode("90600");
				String errorMessage90600 = rp.getValue(JSONHelper.KEY_BASE_ERRORMESSAGE  + "."  + "90600" , "Error in Processing Bills Payment Request");
				errorInterFace.setErrorMessage(errorMessage90600);
				return errorInterFace;
			}


		}

		/*if(response!=null && (busType.equals(OpCodeConstants.BUSINESSTYPE_RRCRENEWAL) || 
				busType.equals(OpCodeConstants.BUSINESSTYPE_RRCAPPLICATION)) 
				&& opCode==OpCodeConstants.ConfirmCode){	

		}*/
		/***********		 * 
		String rs = "" ;

		if(!request.has("TxnType") && request.has("CardNumber")){
			rs = AppTest.readFileByLines(AppTest.EGCGetMember202) ;
		}else{
			String type = request.getString("TxnType");
//			String bt = request.getString(info.getBusinessType());

//			public static final String BUSINESSTYPE_GCSERVER= "GCServer" ;
//			public static final String BUSINESSTYPE_EGCSERVER= "EGCServer" ;
//			public static final String BUSINESSTYPE_EGCSERVER_SELLING = "EGCServerSelling" ;
//			public static final String BUSINESSTYPE_EGCSERVER_TOPUP = "EGCServerTopUp" ;
			 if(type.equals("Confirm")){
				 rs = AppTest.readFileByLines(AppTest.Confirm100) ;
			 }else if(type.equals("SellCoupon")){
				 rs = AppTest.readFileByLines(AppTest.GCSellCoupon101) ;
			}else if(type.equals("RedeemCoupon")){
				 rs = AppTest.readFileByLines(AppTest.GCRedeemCoupon102) ;
			}else if(type.equals("CardSelling")){
				 rs = AppTest.readFileByLines(AppTest.EGCSellCard101) ;
			}else if(type.equals("CardAddValue")){
				rs = AppTest.readFileByLines(AppTest.EGCTopup101) ;
			}else if(type.equals("CardRedeem")){
				 rs = AppTest.readFileByLines(AppTest.EGCRedeemCoupon102) ;
			}
		}

		JSONObject response = new JSONObject(rs);
		 * ****
		 */

		return fillTransaction(info, JSONHelper.toInterface(response));
	}


	private JSONObject requestToSVA(JSONObject request) throws RemoteException, JSONException{
		logger.debug("Sent request: " + request);
		try{
			JSONObject response = svaGateway.request(request);
			logger.debug("Received response: " + response);
			return response;		
		}catch(RemoteException e){
			logger.error("Error on Calling SVA WS. Details: "+e);
			return handleRemoteException(e, request);
		}
	}


	private boolean isNeedToReturnSuccessTxnType(Interface request) throws JSONException{
		Transaction transaction = request.getTransaction() ;
		if(transaction == null){
			return false ;
		}
		String txnType = transaction.getTxnType();
		if(JSONHelper.TXNTYPE_CONFIRM.equals(txnType)){
			return true;
		}
		for(String needTxn : needToRuturnSuccessTxnTypes){
			if(txnType.equals(needTxn)){
				return true;
			}
		}
		return false;
	}

	private boolean isNeedToRuturnSuccessBusTypes(Interface request) throws JSONException{
		String txnType = request.getBusinessType();
		//		if(OpCodeConstants.BUSINESSTYPE_TELECOSERVER.equals(txnType)){
		//			return true;
		//		}
		for(String needTxn : needToRuturnSuccessBusTypes){
			if(txnType.equals(needTxn)){
				return true;
			}
		}
		return false;
	}


	private boolean isNeedToRetryTxnType(JSONObject request) throws JSONException{
		String txnType = request.getString("TxnType");
		if(JSONHelper.TXNTYPE_CONFIRM.equals(txnType)){
			return true;
		}
		for(String needTxn : needToRetryTxnTypes){
			if(txnType.equals(needTxn)){
				return true;
			}
		}
		return false;
	}

	private JSONObject handleRemoteException(RemoteException e, JSONObject request) throws RemoteException, JSONException{
		if(!(e instanceof AxisFault)){
			throw e;
		}
		if(!JSONHelper.ACTION_TRANSACTION.equals(request.getString("Action"))){
			throw e;
		}

		if(!isNeedToRetryTxnType(request)){
			throw e;
		}

		/*if(!JSONHelper.TXNTYPE_CONFIRM.equals(request.getString("TxnType"))){
			return retryToRequestToSVA(request);
		}
		// confirm need to change to ConfirmRecall
		request.put("TxnType", "ConfirmRecall");
		return retryToRequestToSVA(request);*/
		return retryToRequestToSVA(request ,e);
	}

	/*private boolean isNeedToRetry(AxisFault axisFault){
		if(axisFault.getCause() instanceof SocketTimeoutException){
			return true;
		}
		if(axisFault.getCause() instanceof ConnectTimeoutException){
			return true;
		}
		if(axisFault.getMessage().contains("The host did not accept the connection")){
			return true;
		}
		return false;
	}*/

	/**
	 * It only handle SocketTimeoutException
	 * @param request
	 * @return
	 * @throws JSONException 
	 * @throws RemoteException 
	 */
	private JSONObject retryToRequestToSVA(JSONObject request,RemoteException remoteException) throws RemoteException, JSONException{
		RemoteException ex = remoteException;
		for(int i = 0;i < this.retryCountToSVAService; i++){
			try {
				logger.debug("Retry #" + (i+1) + " send " + request.toString());
				JSONObject response = svaGateway.request(request);
				logger.debug("Received response: " + response);
				return response;
			} catch (RemoteException e) {
				if(!(e instanceof AxisFault)){
					throw e;
				}

				logger.warn(e.getMessage(), e);
				ex = e;
				break;
			}
		}
		throw ex;
	}

	private Interface processMemberLogin(Interface info) throws JSONException, RemoteException{
		// the first login first, and return member id.
		JSONObject request = JSONHelper.toJSONObject(info);
		if(this.memberLoginPassword){
			request.put("NoPassword", 1);
		}
		logger.debug("Sent request: " + request);
		JSONObject response = svaGateway.request(request);
		logger.debug("Received response: " + response);
		if(response.getInt("ResponseCode") != 0){
			return JSONHelper.toInterface(response);
		}

		// get member id
		String memberId = response.getString("MemberID");
		String cardNumber = response.getString("CardNumberO");
		logger.debug("CardNumber: " + cardNumber);
		// and then GetMemberInfo
		Interface buffer = new Interface();
		buffer.setOpCode(OpCodeConstants.GetMemberInfoCode);
		Member member = new Member();
		member.setMemberID(memberId);
		buffer.setMember(member);

		request = JSONHelper.toJSONObject(buffer);
		logger.debug("Sent request: " + request);
		response = svaGateway.request(request);
		if(response.has("MemberInfo")){
			JSONObject sub = response.getJSONObject("MemberInfo");
			sub.put("CardNumber", cardNumber);
		}
		logger.debug("Received response: " + response);

		return JSONHelper.toInterface(response);
	}

	private void processEFTPaymentData(Interface data) throws JMSException{
		if(data.getTransaction() == null && data.getRecallTxnNo() == null){
			return ;
		}

		//		if(!"0".equals(data.getResponseCode()))return ;

		if(null == queueConnectionFactory){
			logger.warn("queueConnectionFactory is null, it wouldn't submit transaction data.");
			return ;
		}

		uploadToJMSQueue(data);
	}

	public QueueConnectionFactory getQueueConnectionFactory() {
		return queueConnectionFactory;
	}

	public void setQueueConnectionFactory(
			QueueConnectionFactory queueConnectionFactory) {
		this.queueConnectionFactory = queueConnectionFactory;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public ISVAWebServiceGateway getSvaGateway() {
		return svaGateway;
	}

	public IOperation getSerialization() {
		return serialization;
	}

	public void setSerialization(IOperation serialization) {
		this.serialization = serialization;
	}

	public boolean isMemberLoginPassword() {
		return memberLoginPassword;
	}

	public void setMemberLoginPassword(boolean memberLoginPassword) {
		this.memberLoginPassword = memberLoginPassword;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getStoreCode() {
		return bf.getStoreCode();
	}

	public void setStoreCode(String storeCode) {
		bf.setStoreCode(storeCode);
	}

	public String getBrandCode() {
		return bf.getBrandCode();
	}

	public void setBrandCode(String brandCode) {
		bf.setBrandCode(brandCode);
	}

	public long getPageSize() {
		return bf.getPageSize();
	}

	public void setPageSize(long pageSize) {
		bf.setPageSize(pageSize);
	}

	public long getPageCurrent() {
		return bf.getPageCurrent();
	}

	public void setPageCurrent(long pageCurrent) {
		bf.setPageCurrent(pageCurrent);
	}

	private void uploadToJMSQueue(Interface data) throws JMSException{
		if(queueName == null || "".equals(queueName)){
			final String msg = "JMSTask has to be set \"queueName\" first.";
			logger.error(msg);
			throw new OperationException(msg);
		}

		if(!isConnectToActiveMQ()){
			synchronized(locker){
				try{
					connectAndInitActiveMQ();
				}catch(JMSException e){
					logger.error(e.getMessage(), e);
					retryToConnectActiveMQ();
				}
			}
		}


		messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);

		logger.info("Created  MessageProducer:" + messageProducer.toString() + " successfully.");
		messageProducer.send(session.createObjectMessage(data));
		session.commit();
		logger.debug("sent successfully.");
	}

	private synchronized boolean isConnectToActiveMQ(){
		if(null == connection){
			return false;
		}
		return true;
	}

	private void connectAndInitActiveMQ() throws JMSException{
		logger.debug("It's ready to connect to jms service");
		if(null != connection){
			try{
				connection.close();
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}
		}
		connection = queueConnectionFactory.createConnection();
		connection.start();
		logger.debug("Connected successfully: " + connection);

		session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		queue = session.createQueue(queueName);
		messageProducer = session.createProducer(queue);
	}

	private void retryToConnectActiveMQ() throws JMSException{
		JMSException lastE = null;
		logger.warn("++++++retry to connect to activemq, total count is: " + retryCount + "+++++");
		for(int i = 0;i < retryCount;i++){
			try{
				logger.debug("Retrying #" + (i + 1) + " connectting.");
				connectAndInitActiveMQ();
				logger.debug("Retry successfully.");
				return ;
			}catch(JMSException e){
				lastE = e;
				logger.error(e.getMessage());
			}
		}
		logger.warn("++++++retry ended++++++");
		if(null != lastE)
			throw lastE;
	}

	private Interface fillStoreCode(Interface request){
		if(this.getStoreCode() == null)return request;
		if(request.getTransaction() == null)return request;
		String oldStoreCode = request.getTransaction().getStoreId();
		//		if(oldStoreCode == null || oldStoreCode.equals(""))
		request.getTransaction().setStoreId(this.getStoreCode());
		logger.debug("Set store code(" + oldStoreCode + ") to " + request.getTransaction().getStoreId());
		return request;
	}

	private Interface fillBrandCode(Interface request){
		if(this.getBrandCode() == null)return request;
		if(request.getTransaction() == null)return request;
		String oldBrandCode = request.getTransaction().getBrandCode();
		request.getTransaction().setBrandCode(this.getBrandCode());
		logger.debug("Set store code(" + oldBrandCode + ") to " + request.getTransaction().getBrandCode());
		return request;
	}

	private Interface fillPage(Interface request){
		if(request.getTransaction() != null)return request;
		Page page = request.getPage();
		if(null == page){
			page = new Page();
			page.setPageCurrent(1L);
			page.setPageSize(10L);
		}
		long oldPageCurrent = page.getPageCurrent();
		long oldPageSize = page.getPageSize();
		page.setPageCurrent(getPageCurrent());
		page.setPageSize(getPageSize());
		request.setPage(page);
		logger.debug("Set page current(" + oldPageCurrent + ") to " +page.getPageCurrent() + " ; " +"Set page size(" + oldPageSize + ") to " +page.getPageSize() );
		return request;
	}

	/**
	 * fill request of Interface of Transaction with response of Interface of Transaction.
	 * response's data covert to request if some property exists.
	 * @param request
	 * @param response
	 * @return
	 */
	private Interface fillTransaction(Interface request, Interface response){
		response.setOpCode(request.getOpCode());
		response.setBusinessType(request.getBusinessType());
		//text
		//		response.setResponseCode("0") ;
		//te
		if(null != request.getRecallTxnNo()){
			response.setRecallTxnNo(request.getRecallTxnNo());
			return response;
		}
		if(response.getCoupon() != null && request.getCoupon() != null){
			if(response.getCoupon().getCouponUID() == null){
				response.getCoupon().setCouponUID(request.getCoupon().getCouponUID());
			}
		}
		if(request.getTransaction() == null)return response;
		if(!"0".equals(response.getResponseCode()))return response;
		Transaction requestTxn = request.getTransaction();
		Transaction responseTxn = response.getTransaction();
		if(responseTxn == null)return response;

		if(responseTxn.getApprovalCode() == null){
			responseTxn.setApprovalCode(requestTxn.getApprovalCode());
		}
		if(responseTxn.getTotalAmount() == null){
			responseTxn.setTotalAmount(requestTxn.getTotalAmount());
			//			responseTxn.setTotalAmount(requestTxn.getTotalAmount());
		}
		if(responseTxn.getItemUnitAmount() == null){
			responseTxn.setItemUnitAmount(requestTxn.getItemUnitAmount());
		}
		if(responseTxn.getItemCount() == null){
			responseTxn.setItemCount(requestTxn.getItemCount());
		}
		if(responseTxn.getItemUID() == null){
			String uid = requestTxn.getItemUID();
			responseTxn.setItemUID(uid);
		}

		if(responseTxn.getMobileNumber() == null){
			String mobileNumber = requestTxn.getMobileNumber();
			responseTxn.setMobileNumber(mobileNumber);
		}

		if(responseTxn.getSKU() == null){
			responseTxn.setSKU(requestTxn.getSKU());
		}

		if(responseTxn.getTenderId() == null){
			responseTxn.setTenderId(requestTxn.getTenderId());
		}

		if(requestTxn.getTxnType() != null){
			responseTxn.setTxnType(requestTxn.getTxnType());
		}

		//		if(responseTxn.getTxnType() == null){
		//			responseTxn.setTxnType(requestTxn.getTxnType());
		//		}

		if(responseTxn.getTargetSN() == null){
			responseTxn.setTargetSN(requestTxn.getTargetSN());
		}

		if(responseTxn.getBalance() == null){
			responseTxn.setBalance(requestTxn.getBalance());
		}
		if(responseTxn.getAmount() == null){
			responseTxn.setAmount(requestTxn.getAmount());
		}

		if(responseTxn.getMobileNumber() == null){
			responseTxn.setMobileNumber(requestTxn.getMobileNumber());
		}
		if(responseTxn.getForfeitAmount() == null){
			responseTxn.setForfeitAmount(requestTxn.getForfeitAmount());
		}
		if(responseTxn.getCouponAmount() == null){
			responseTxn.setCouponAmount(requestTxn.getCouponAmount());
		}
		if(responseTxn.getBusDate() == null){
			responseTxn.setBusDate(requestTxn.getBusDate());
		}
		if(responseTxn.getCardNumber() == null){
			responseTxn.setCardNumber(requestTxn.getCardNumber());
		}
		if(responseTxn.getCardStatus() == null){
			responseTxn.setCardStatus(requestTxn.getCardStatus());
		}
		if(responseTxn.getCardType() == null){
			responseTxn.setCardType(requestTxn.getCardType());
		}
		if(responseTxn.getCashierId() == null){
			responseTxn.setCashierId(requestTxn.getCashierId());
		}
		if(responseTxn.getCouponCount() == null){
			responseTxn.setCouponCount(requestTxn.getCouponCount());
		}
		if(responseTxn.getCouponStatus() == null){
			responseTxn.setCouponStatus(requestTxn.getCouponStatus());
		}
		if(responseTxn.getCouponUID() == null){
			responseTxn.setCouponUID(requestTxn.getCouponUID());
		}
		if(responseTxn.getDescription() == null){
			responseTxn.setDescription(requestTxn.getDescription());
		}
		if(responseTxn.getExpiryDate() == null){
			responseTxn.setExpiryDate(requestTxn.getExpiryDate());
		}
		if(responseTxn.getPosId() == null){
			responseTxn.setPosId(requestTxn.getPosId());
		}
		if(responseTxn.getServerId() == null){
			responseTxn.setServerId(requestTxn.getServerId());
		}
		if(responseTxn.getSN() == null){
			responseTxn.setSN(requestTxn.getSN());
		}
		if(responseTxn.getStoreId() == null){
			responseTxn.setStoreId(requestTxn.getStoreId());
		}
		if(responseTxn.getTxnDate() == null){
			responseTxn.setTxnDate(requestTxn.getTxnDate());
		}
		if(responseTxn.getTxnNo() == null){
			responseTxn.setTxnNo(requestTxn.getTxnNo());
		}
		if(responseTxn.getTxnType() == null){
			responseTxn.setTxnType(requestTxn.getTxnType());
		}
		if(responseTxn.getUserId() == null){
			responseTxn.setUserId(requestTxn.getUserId());
		}
		if(responseTxn.getVoidTxnDate() == null){
			responseTxn.setVoidTxnDate(requestTxn.getVoidTxnDate());
		}
		if(responseTxn.getOriginalTxnNo() == null){
			responseTxn.setOriginalTxnNo(requestTxn.getOriginalTxnNo());
		}
		if(responseTxn.getBrandCode() == null){
			responseTxn.setBrandCode(requestTxn.getBrandCode());
		}

		if(responseTxn.getCouponDeductAmount() == null){
			responseTxn.setCouponDeductAmount(requestTxn.getAmount());
		}
		// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate +++
		if(responseTxn.getAuthDate() == null){
			responseTxn.setAuthDate(requestTxn.getAuthDate());
		}
		if(responseTxn.getConfirmDate() == null){
			responseTxn.setConfirmDate(requestTxn.getConfirmDate());
		}
		// By Ivan.WONG 2016-09-08: add AuthDate, ConfirmDate ---

		//		responseTxn.setAllowedTenders("1.2.3");
		//		responseTxn.setCustomerReceipt("Customer Receipt");
		//		responseTxn.setStoreReceipt("Store Receipt");
		//		responseTxn.setEjReceipt("Ej Receipt");
		return response;
	}


	public ArrayList<Transaction> parseGlobeTranstarget(ArrayList<Transaction> list, JSONArray targetTrans){
		int ii =0;
		String s = null;
		while(ii<targetTrans.length()){
			Transaction txn = new Transaction();
			try {
				s = (String) targetTrans.get(ii);
				logger.debug("targetTrans: "+s);
			} catch (JSONException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}

			String[] transList = s.split("\"|\\|");
			for(int i=0; i<transList.length; i++){
				txn.setMobileNumber(transList[6]);
				txn.setProdCode(transList[3]);
				txn.setTxnDate(transList[2]);
				txn.setReferenceNo(transList[0]);
				txn.setTxnStatus(Integer.parseInt(transList[4]));
			}
			logger.debug("MobileNumber: "+txn.getMobileNumber()+
					" ProdCode: "+txn.getProdCode()+
					" TxnDate: "+txn.getTxnDate()+
					" ReferenceNo: "+txn.getReferenceNo()+
					" TxnStatus: "+txn.getTxnStatus());
			list.add(txn); 
			ii++;
		}
		return list; 
	}
	public ArrayList<Transaction> parseGlobeTranstarget(ArrayList<Transaction> list, String[] targetTrans){
		int ii =0;
		String s = null;
		while(ii<targetTrans.length){
			Transaction txn = new Transaction();
			s = (String) targetTrans[ii];
			logger.debug("targetTrans: "+s);

			String[] transList = s.split("\"|\\|");
			for(int i=0; i<transList.length; i++){
				txn.setMobileNumber(transList[6]);
				txn.setProdCode(transList[3]);
				txn.setTxnDate(transList[2]);
				txn.setReferenceNo(transList[0]);
				txn.setTxnStatus(Integer.parseInt(transList[4]));
			}
			logger.debug("MobileNumber: "+txn.getMobileNumber()+
					" ProdCode: "+txn.getProdCode()+
					" TxnDate: "+txn.getTxnDate()+
					" ReferenceNo: "+txn.getReferenceNo()+
					" TxnStatus: "+txn.getTxnStatus());
			list.add(txn); 
			ii++;
		}
		return list; 
	}

	public ArrayList<Transaction> parseGlobeTranstarget(ArrayList<Transaction> list, String targetTrans){
		targetTrans = targetTrans.replace("[", "");
		targetTrans = targetTrans.replace("]", "");

		String[] strArr = targetTrans.split(",");
		String s ="";


		for (int i =0; i<strArr.length; i++) {
			Transaction txn = new Transaction();

			s = strArr[i].replace("\"", "");
			String[] sub = s.split("\\|");
			txn.setMobileNumber(sub[6]);
			txn.setProdCode(sub[3]);
			txn.setTxnDate(sub[2]);
			txn.setReferenceNo(sub[0]);
			txn.setTxnStatus(Integer.parseInt(sub[4]));
			list.add(txn); 
		}


		return list; 
	}
	private static void sortList(List<Transaction> list) {
		Collections.sort(list, new Comparator<Transaction>() {

			@Override
			public int compare(Transaction o1, Transaction o2) {
				String idea1 = o1.getTxnDate();
				String idea2 = o2.getTxnDate();
				return idea2.compareTo(idea1);
			}


		});
	}

	public ArrayList<Transaction> parseSmartInquiryResult(
			ArrayList<Transaction> list, String targetTrans) {
		String result = "";
		try {
			result = targetTrans;
			if(!result.isEmpty()){
				result = result.replace("[\"", "");
				result = result.replace("\"]", "");
				result = result.replace("\\", "");
			}
			JSONObject o = new JSONObject(result);

			String txnStatus = (String) o.get(OpCodeConstants.KEY_RESPCODE);
			/*
			if (txnStatus != null)
				txnStatus = txnStatus.equals("0000") ? "0" : txnStatus;
			 */
			Transaction txn = new Transaction();
			txn.setMobileNumber((String) o
					.get(OpCodeConstants.KEY_TARGETSUBSACCOUNT));
			txn.setProdCode((String) o.get(OpCodeConstants.KEY_PLANCODE));
			txn.setReferenceNo((String) o
					.get(OpCodeConstants.KEY_TRANSACTIONRRN));
			txn.setTxnStatus(Integer.parseInt(txnStatus));


			String txnDate = (String) o.get(OpCodeConstants.KEY_TRANSACTIONTIMESTAMP);

			SimpleDateFormat sdf = new SimpleDateFormat();
			String pattern = "MMM-dd-yyyy HH:mm:ss";
			sdf.applyPattern(pattern);
			String dateFormatted="";
			if(txnDate!=null && !txnDate.isEmpty()){
				try{
					long longDate = new Long(txnDate).longValue();
					dateFormatted=sdf.format(new Date(longDate));
					logger.info("LongDate: "+longDate);
					logger.info("FormattedDate: "+dateFormatted);

				}catch(Exception e){
					e.printStackTrace();
					logger.error(e.getMessage());
				}
			}
			txn.setTxnDate(dateFormatted);



			logger.debug("MobileNumber: " + txn.getMobileNumber()
					+ " ProdCode: " + txn.getProdCode() + " TxnDate: "
					+ txn.getTxnDate() + " ReferenceNo: "
					+ txn.getReferenceNo() + " TxnStatus: "
					+ txn.getTxnStatus());
			list.add(txn);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}
	public String getErrorCode(String targetTransRes){
		String result = targetTransRes;
		String respCode="";
		if(!result.isEmpty()){
			result = result.replace("[\"", "");
			result = result.replace("\"]", "");
			result = result.replace("\\", "");
		}
		try {
			JSONObject o = new JSONObject( result);
			logger.info("result to string: " + o.toString());
			respCode = (String) o.get(OpCodeConstants.KEY_RESPCODE);
		} catch (JSONException e) {
			respCode="90504";
		}




		return respCode;

	}

	private String getBusinessDateFromStorelineFile(){
		try{
			String busDateRaw = null;

			FileReader fr = new FileReader(rp.getValue("Storeline.Busdate"));
			BufferedReader br = new BufferedReader(fr);
			try {
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null) {
					sb.append(line);
					sb.append("\n");
					line = br.readLine();
				}
				busDateRaw = sb.toString();
			} finally {
				fr.close();
				br.close();
			}

			String decadeId = busDateRaw.substring(6, 7);
			String year = busDateRaw.substring(7,8);
			String month = busDateRaw.substring(8, 10);
			String day = busDateRaw.substring(10, 12);

			String decade = rp.getValue("Storeline.Busdate.Decade."+decadeId);

			return decade+year+month+day;

		}catch(Exception e){
			logger.error(e,e);
		}	
		return null;
	}
}
