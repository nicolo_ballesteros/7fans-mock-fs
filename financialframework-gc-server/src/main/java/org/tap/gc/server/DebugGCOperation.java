package org.tap.gc.server;

import javax.xml.bind.JAXBException;

import org.tap.communication.IPacket;
import org.tap.communication.Receipt;
import org.tap.gc.XmlHelper;
import org.tap.gc.beans.Interface;
import org.tap.gc.beans.Movement;

import com.tap.operation.OperationBase;
import com.tap.operation.OperationException;

public class DebugGCOperation extends OperationBase {

	@Override
	public Object operate(Object info) {
		if (!(info instanceof IPacket))
			throw new IllegalArgumentException(
					"info is not an instance of IPacket.");
		IPacket packet = (IPacket) info;
		Interface response;
		try {
			response = process((Interface) XmlHelper
					.unmarshaller((String) packet.getReceived()));
			Receipt receipt = new Receipt(packet,
					XmlHelper.marshaller(response));
			receipt.setBusiness("GC");
			receipt.setAction(response.getOpCodeDescription());
			return receipt;
		} catch (JAXBException e) {
			throw new OperationException(e);
		}
	}
	
	private Interface process(Interface info) {
		info.setResponseCode("00");
		if(info.getTransaction() != null){
			info.getTransaction().setApprovalCode("000001");
			info.getTransaction().setExpiryDate("2012-10-11");
			info.getTransaction().setBalance(Long.parseLong("10000"));
		}else if(info.getCoupon() != null){
			info.getCoupon().setCouponExpiryDate("2111-07-02");
			info.getCoupon().setCouponNumber("801952030002");
			info.getCoupon().setCouponStatus("1");
			info.getCoupon().setCouponTypeAmount(Long.parseLong("50000"));
			info.getCoupon().setCouponAmount(Long.parseLong("50000"));
			info.getCoupon().setCouponTypeCode("UGC000500");
			info.getCoupon().setCouponTypeID("19");
			info.getCoupon().setCouponTypeName("UNIVERSAL GC 500");
			info.getCoupon().setCouponTypePoint(Long.parseLong("0"));
			info.getCoupon().setCouponTypeDiscount(Long.parseLong("100"));
			info.getCoupon().setCouponTypeNotes("");
		}else if(info.getMovements() != null){
			Movement m = new Movement();
			m.setAmount(Long.parseLong("10000"));
			m.setBatchCouponID("10000");
			m.setCardNumber("1234567890");
			m.setCouponActiveDate("2011-7-5");
			m.setCouponExpiryDate("2013-7-5");
			m.setCouponIssueDate("2011-7-5");
			m.setCouponNumber("1000001");
			m.setCouponStatus(0);
			m.setOprID("1");
			m.setRefTxnNo("1234567890");
			m.setStoreID("1");
			info.getMovements().getMovement().add(m);
			
			m = new Movement();
			m.setAmount(Long.parseLong("20000"));
			m.setBatchCouponID("10001");
			m.setCardNumber("1234567890");
			m.setCouponActiveDate("2011-7-4");
			m.setCouponExpiryDate("2013-7-4");
			m.setCouponIssueDate("2011-7-4");
			m.setCouponNumber("1000002");
			m.setCouponStatus(0);
			m.setOprID("2");
			m.setRefTxnNo("1234567890");
			m.setStoreID("888");
			info.getMovements().getMovement().add(m);
		}
		return info;
	}

}
