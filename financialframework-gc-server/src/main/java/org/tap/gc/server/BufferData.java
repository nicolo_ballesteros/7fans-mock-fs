package org.tap.gc.server;

/**
 * It will buffer some data into memory. for others object to access
 * @author Jacky.LIU
 *
 */
public class BufferData {

	private volatile  static BufferData self;
	
	private String storeCode;
	
	private String brandCode;
	
	private long pageCurrent = 1;
	
	private long pageSize = 10;
	
	BufferData(){}
	
	public static BufferData getInstance(){
		synchronized(BufferData.class) {
			if(null == self)
				self = new BufferData();
			}
		return self;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public long getPageCurrent() {
		return pageCurrent;
	}

	public void setPageCurrent(long pageCurrent) {
		this.pageCurrent = pageCurrent;
	}

	public long getPageSize() {
		return pageSize;
	}

	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	
}
