package org.tap.gc.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.tap.communication.IPacket;
import org.tap.gc.ErrorCode;
import org.tap.gc.security.ISecurity;
import org.tap.gc.security.support.NonSecurity;

import com.tap.operation.IOperation;
import com.tap.operation.OperationBase;

/**
 * It will handle IPacket from TCPListener, and return IReceipt to TCPListener.
 * It will be load into FS service
 * 
 * 
 */
public class BusinessChoiceOperation extends OperationBase {
	
	private Map<String , IOperation> businessMap = new HashMap<String, IOperation>();

	private ISecurity security;
	
	private String encoding = "UTF-8";
	
	private final String DEFAULT_OPERATION = "DefaultOperation" ;
	
	private static Logger logger = Logger.getLogger(BusinessChoiceOperation.class);
	
	public BusinessChoiceOperation(Map<String , IOperation> businessMap){
		if(businessMap != null){
			this.businessMap = businessMap ;
		}
	}
	
	@Override
	public Object operate(Object info) {
		
		if (!(info instanceof IPacket))
			throw new IllegalArgumentException(
					"info is not an instance of IPacket.");
		IPacket packet = (IPacket) info;
		try {
			String requestXml = getSecurity().decode((String) packet.getReceived(), encoding);
			Set<Entry<String, IOperation>> set = this.businessMap.entrySet();
			Iterator<Entry<String, IOperation>> iter = set.iterator() ;
			while(iter.hasNext()){
				Map.Entry<String, IOperation> entry1 =  iter.next();
				String key = entry1.getKey();
				if(requestXml.contains(key)){
					IOperation operation = entry1.getValue();
					return operation.operate(info);
				}
			}
			if(this.businessMap.containsKey(DEFAULT_OPERATION)){
				IOperation operation = this.businessMap.get(DEFAULT_OPERATION);
				return operation.operate(info);
			}else{
				try {
					packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_SYSTEMERROR));
				} catch (JAXBException e1) {
					e1.printStackTrace();
				}
			}
		}catch(Exception e){
			try {
				packet.getSender().operate(ErrorCode.getErrorResponse(packet, ErrorCode.CODE_SYSTEMERROR));
			} catch (JAXBException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	public ISecurity getSecurity() {
		if(null == security)
			security = new NonSecurity();
		return security;
	}
	
	public void setSecurity(ISecurity security) {
		this.security = security;
	}
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
}
