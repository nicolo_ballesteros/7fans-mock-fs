package org.tap.gc.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class AppTest {
	
	public static File EGCGetMember202 = new File("EGCGetMember202.xml") ;
	
	public static File Confirm100 = new File("./src/test/resources/Confirm100.xml") ;
	public static File GCRedeemCoupon102 = new File("./src/test/resources/GCRedeemCoupon102.xml") ;
	public static File GCSellCoupon101 = new File("./src/test/resources/GCSellCoupon101.xml") ;
	
	public static File EGCSellCard101 = new File("./src/test/resources/EGCSellCard101.xml") ;
	public static File EGCTopup101 = new File("./src/test/resources/EGCTopup101.xml") ;
	public static File EGCRedeemCoupon102 = new File("./src/test/resources/EGCRedeemCoupon102.xml") ;
	
	public static String readFileByLines(File file) {
		StringBuffer sb = new StringBuffer();
		BufferedReader reader = null;
		String s = "";
		try {
			reader = new BufferedReader(new FileReader(file));
			s = reader.readLine();
			while (s != null) {
				sb.append(s);
				sb.append("\r\n");
				s = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

}
